<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AccountController extends Controller
{
    public function recorverPasswordConfirm(Request $request, $link){
        //if($request->isMethod('get')){
            //this assumes that a get request is sent to the server
            //var_dump($request->rpassword);
            $data = array('link'=>$link);
           $url = 'http://api.hevently.com/v1/user/';
           $recorver = new PostToApi($url, $data);
                       $recorver->get();
          $response = $recorver->response;
          $jsonResponse = json_encode($response);
          $jsonRespons['status'] = 'success';
          if(!isset($jsonResponse['status']) && $jsonRespons['status'] == 'success'){
              //then redirect to change password route
              //get the passowrd changed
              //within this function we'll allow change of password
              //first of all validate the pasword
              if($request->has('password') && $request->has('rpassword')){
                  if($request->password == $request->rpassword){
                        $changePassword = new UserController();
                        $responsePwd = $changePassword->changePassword($link, 'pwd_rec', $request->password);
                        if($responsePwd['status']){
                            $data = $this->passwordSuccess();
                            return view('reg.success', compact('data'));
                        }else{
                            $data = array('sucess'=>false,'reason'=>$responsePwd['reason'],'fields'=>[], 'inputs'=>$request);
                            return View('password_rec')->withData($data);   
                  
                        }
                        return view('password_rec');
                  }else{
                      
                $data = array('sucess'=>false,'reason'=>'Password mis-match','fields'=>[], 'inputs'=>$request);
                return View('password_rec')->withData($data);   
                  }
              }else{
                $data = array('sucess'=>false,'reason'=>'Field(s) left blank','fields'=>[], 'inputs'=>$request);
                return View('password_rec')->withData($data);   
              }
          }else{
              //display error
              $data = (isset($jsonResponse['data']['message']) ? $jsonResponse['data']['message'] : 'Uncatchable error');
              return view('reg.error')->withData($data);
          }
       /* }else
        return redirect('/');
        */
    }
   public function recorverPassword(Request $request = null){
       //set recorver passsword
       if(!$request->has('email'))
       return View('password_rec')->withData($email = '');
       else{
           //then ther is a post request from the user
           if($request->has('email')){
               //then send a post request to the API
           $data = array('email'=>$request->email);
           $url = 'http://api.hevently.com/v1/user/password/forgot';
           $recorver = new PostToApi($url, $data);
                       $recorver->post();
          $response = $recorver->response;
           
           $getTO = json_decode($response, true);
           if($getTO['status'] == 'success'){
               //if successfull then redirect to success page
               $data = $this->passwordRecTemplate($request->email);
               return view('reg.success', compact('data'));
           }else{
               $data = array('sucess'=>false,'reason'=>(isset($getTO['data']) ? $getTO['data']['message'] : 'could not connect to the server'),'fields'=>[], 'inputs'=>$request);
               return View('password_rec')->withData($data);    
           }
           }else{
             $data = $this->passwordRecTemplate();
             return View('password_rec')->withData($data);
           }
       }
   }
   
   public function passwordRecTemplate($email){
       return '
    <div class="main">
        <div class="main-inner">
            <div class="container">
                <div class="content">
                    


                    <div class="row">
    <div class="col-sm-4 col-sm-offset-4" style="background:#fefefe;padding:2% ">
        	<div align="center">
				<div class="container-fluid bg-1 text-center">
  					<img src="right.jpg" class="" style="display: inline; width: 40px; height: 40px;" alt="Bird">
                      <h2> <span class="text-success">password Recorvery</span></h2>
 					 <img src="success1.jpg" class="img-circle" alt="Bird">  
                      <h3><span class="text-danger">Confirm Action!</span></h3>
 					 <h4><span class="text-info">A confirmatory link has been sent to '.$email.'</span></h4>
				</div>
			</div>
    </div><!-- /.col-sm-4 -->
</div><!-- /.row -->

                </div><!-- /.content -->
            </div><!-- /.container -->
        </div><!-- /.main-inner -->
    </div><!-- /.main -->';
   }
    public function passwordSuccess($email = null){
       return '
    <div class="main">
        <div class="main-inner">
            <div class="container">
                <div class="content">
                    


                    <div class="row">
    <div class="col-sm-4 col-sm-offset-4" style="background:#fefefe;padding:2% ">
        	<div align="center">
				<div class="container-fluid bg-1 text-center">
  					<img src="right.jpg" class="" style="display: inline; width: 40px; height: 40px;" alt="Bird">
                      <h2> <span class="text-success">password Recorvery</span></h2>
 					 <img src="success1.jpg" class="img-circle" alt="Bird">  
                      <h3><span class="text-success">Password Changed!</span></h3>
 					 <h4><span class="text-info">Your password has successfully been changed</span></h4>
				</div>
			</div>
    </div><!-- /.col-sm-4 -->
</div><!-- /.row -->

                </div><!-- /.content -->
            </div><!-- /.container -->
        </div><!-- /.main-inner -->
    </div><!-- /.main -->';
   }
}
