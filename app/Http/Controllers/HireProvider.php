<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class HireProvider extends Controller
{
    //this will process the hire me request
    public function hire($provider = '', Request $request){

        $provider = ($provider =='') ? $request->get('provider_link') : $provider;
        //get the provider's ID
        $getIt = new SPProfileEdit();
        $provider;
        $info = $getIt->loadProviderInfo($provider);
        $info2 = json_decode($info, true);
        $provider_id = $info2['data']['_id']['$id'];
        $data = array();
        $data['title'] = 'Hire '.$info2['data']['bizname'];
        $data['message'] = "You've successfully sent a hire request to ".$info2['data']['bizname'];
        $data['danger'] = 'Please wait for '.$info2['data']['bizname'].' to reply you';
        return View('reg.success2')->withData($data);
    }
}
