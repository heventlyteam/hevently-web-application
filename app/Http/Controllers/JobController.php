<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class JobController extends Controller
{   public $url = 'http://api.hevently.com/v1/';
    //
    public function getAllJobs(){
        //this will get all the jobs available in the db
        $url = $this->url.'work';
        $data = array();
        $post = new PostToApi($url, $data);
                $post->get();
        $response = $post->response;
        return $response;

    }
    public function getJob(Request $request, $id = ''){
        $jobID = ($id != '') ? $id : $request->get('job_id');
        $getReq = new PostToApi($this->url.'/work/'.$jobID, array());
                $getReq->get();
        $response = $getReq->response;
        return $response;
    }
    public function postJob(Request $request){
        //this will be responsible for posting jobs
        //check if the is a post request
        if($request->method() == 'POST'){
            //if the request method is post then validate and
            //checkif all the data are available
            if($request->has('profession_needed') &&
            $request->has('work_about') &&
            $request->has('min_budget') &&
            $request->has('max_budget') &&
            $request->has('deadline') &&
                $request->has('longitude') &&
                $request->has('latitude')){
                $sessionInfo = new \Illuminate\Support\Facades\Session();
                $userInfo = $sessionInfo::get('userinfo');
                $username = $userInfo->username;
                $professiona_needed = $request->get('profession_needed');
                //echo 'ww';
                $about_work = $request->get('work_about');
                $min_budget = $request->get('min_budget');
                $max_budget = $request->get('max_budget');
                $deadline = $request->get('deadline');
                $latitude = $request->get('latitude');
                $longitude = $request->get('longitude');
                $event_info = $request->get('event_info');

                $data1 = $this->getStep1();
                $data = json_decode($data1, true);
                //post posted data to the api
                $dataPost = array('username'=>''.$username.'', 'job'=>'
    {
    "title":"'.$professiona_needed.'",
    "event_type":"'.$about_work.'",
    "budget":"'.$min_budget.' '.$max_budget.'",
    "expected_number_of_people":"",
    "hall_size":"3000",
    "type_of_food":"rice, jollof rice",
    "description": "'.$event_info.'"
    }','profession'=>''.$professiona_needed.'', 'category'=>$professiona_needed, 'longitude'=>$longitude, 'latitude'=>$latitude, 'message'=>$event_info, "title"=>$professiona_needed, "event_type"=>$about_work,
    "budget (Naira)"=>$min_budget.' '.$max_budget.'',);
               $post = new PostToApi($this->url.'work', $dataPost);
                        $post->post();
                $response = $post->response;
                $data['inputs'] = $request;
                $data['response'] = $response;
                if($data['status'] == 'success'){
                    unset($data);
                    $data['title'] = 'Job Post successful';
                    $data['warning'] = '';
                    $data['message'] = 'Your job has been successfully posted you can check your messages for applications from related Service providers';
                    return View('reg/success2')->withData($data);
                }
                return View('post_job')->withData($data);
            }else{
                $data1 = $this->getStep1();
                $data = json_decode($data1, true);
                $data['error'] = 'No field can be left blank';
                return View('post_job')->withData($data);
            }
        }else{
            //if there is no post method then show the blade
            //first of all load the first step
           $data1 = $this->getStep1();
           $data = json_decode($data1, true);
           return View('post_job')->withData($data);
        }
        // if ther is a post request then
    }

    public function getStep1(){
        //send a post request to load all the available service providers
        $getPro = new ProfessionalsController();
        $requestReturn = $getPro->getProfessionals();
        return $requestReturn;
    }
    public function apply(Request $request, $id=''){
        //this is to apply for a job

        $jobID = ($id != '') ? $id : $request->get('job_id');
        //get the job ID
        $getIT = new JobController();
        $getRes = $getIT->getJob($request, $jobID);
        $dd = json_decode($getRes,true);
        if($dd['status'] != 'success')
            return View(error, 'Cou;d not connect to the internet');
        $jobID = $dd['data']['_id']['$id'];
        $sessionInfo = new \Illuminate\Support\Facades\Session();
        $userInfo = $sessionInfo::get('sp_info');
        $data = array('work_id'=>$jobID, 'provider_id'=>$userInfo->user_id,'proposal'=>'jd iuygdfyugh');
        $postTo = new PostToApi($this->url.'work/apply', $data);
                $postTo->post();
        $response = $postTo->response;
        $res = json_decode($response, true);
        if($res['status'] == 'success'){
            unset($data);
            $title= 'Apllication for Work';
            $danger = 'Please wait for a message from the user';
            $message = 'Your application has been sent to the user';
            $data = array('title'=>$title, 'danger'=>$danger, 'message'=>$message);
            return View('reg.success2')->withdata($data);
        }else{
            unset($data);
            $data = 'Sorry we could not process your request because: <br /> '.$res['message'];
            return View('reg.error')->withData($data);
        }
    }
}
