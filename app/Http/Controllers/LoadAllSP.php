<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class LoadAllSP extends Controller
{
    //the function of this is to load all sps on the serve
    public $url = 'http://api.hevently.com/v1';
    public function loadAll(){
        //post to all
        $url = $this->url;
        $data = array();
        $postTo = new PostToApi($url.'/providers', $data);
                    $postTo->get();
        $response = $postTo->response;
        //now decode the json response
        $responseDecoded = json_decode($response, true);
        //now return all the loaded providers
       return $responseDecoded;
    }
}
