<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class LoadReviews extends Controller
{
    //load reviews
    private $url = 'http://api.hevently.com/v1';
    public function allReviews(){
        $data = array('');
        $postTo = new PostToApi($this->url.'/reviews', $data);
                $postTo->get();
        $response = $postTo->response;
        //json decode response
        $responseDecode = json_decode($response, true);
        return $responseDecode;
    }

    public function AddReview(Request $request){
        //ajax request is expected
        if($request->getMethod() == 'POST'){
            if($request->has('id') && $request->has('review')){
                $data2Send = array('id'=>$request->get('id'),'review'=>$request->get('review'));
                $postTo = new PostToApi($this->url.'/review', $data2Send);
                            $postTo->post();
                $response = $postTo->response;
                $decodedResponse = json_decode($response, true);
                if($decodedResponse['status'] == 'success'){
                    $return = array('status'=>true, 'message'=>'Your review has successfully been added');
                }else{

                    $return = array('status'=>false, 'message'=>'replace when connected');
                }

            }else{
                return redirect('');
            }
        }
    }




}
