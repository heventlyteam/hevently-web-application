<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class LoginSessionManagement extends Controller
{
   //the main function of this controller is to make create session for login using the gotten details from the
   //yepp
   private $userINFO;
   private $sessionName;
    public function setAll($json, $admin=null){
      if($admin ==  null)
      $c = new UserInfo();
      else
      $c = new SPInfo();
      
      $c->setAll($json);
      $this->userINFO = $c;
    }
    public function setSessionName($sessionName){
        $this->sessionName = $sessionName;
    }
    public function createSession(){
        //this will create the session 
        $session = new \Session();
        $session::put($this->sessionName, $this->userINFO);
    }
    public static function validateLoginSession($sessionName=null){
        //this function returns true if the login id valid and false if otherwise
        $val = false;
        $session = new \Session();
       if($session::has($sessionName)){
           //now validate the userinfo
           $val = true;
       }else{
           $val = false;
       }
        return $val;
    }
   public function logout(){
        $session = new \Session();
        $session::forget($this->sessionName);
   }
}
