<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use GuzzleHttp\Exception\RequestException;
use League\Flysystem\Exception;

class PostToApi extends Controller
{
    private $url;
    private $data;
    public $response;
   public function __construct($url, $data){
       $this->url = $url;
       $this->data = $data;
   }

   public function post($file = null){
       $url = $this->url;
       $data = $this->data;
      // var_dump($data);
       //this sends a post request to the API
       $client = new \GuzzleHttp\Client();
        try{

           if($file == null)
                $res = $client->request('POST', $url,
                                        [
                                            'form_params'=> $data,
                                        ['http_errors' => true]
                ]);
            else
                $res =   $client->request('POST', $url, $data);

         $jsonResponse  = $res->getBody()->getContents();
        }catch(ClientException $ex){

            $jsonResponse = $ex->getResponse()->getBody()->getContents();
        }catch(RequestException $ex){
            $code = $ex->getResponse()->getStatusCode();
            if($ex->getResponse()->getStatusCode() == '400') {
                $jsonResponse = $ex->getResponse()->getBody()->getContents();
            }else if($ex->getResponse()->getStatusCode() == '500')
                $jsonResponse = $ex->getResponse()->getBody()->getContents();
            else
            dd('Oops! Internet connection got wrong somewhere...');

        }catch(ConnectException $ex){
            dd('Oops! Server down or invalid request');
        }catch(Exception $ex){
            dd('Please reload page to fix');
        }
       //echo $jsonResponse;
        $this->response = $jsonResponse;
   }

   public function get(){
       $url = $this->url;
       $data = $this->data;
       //this sends a post request to the API
       $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url,
                                    ['form_params'=>
                                            $data,
                                    ['http_errors' => true]
            ]);

           $jsonResponse  = $res->getBody()->getContents();
        }catch(ClientException $ex){

            $jsonResponse = $ex->getResponse()->getBody()->getContents();
        }catch(RequestException $ex){
            if ($ex->getResponse()->getStatusCode() == '400') {
                $jsonResponse = $ex->getResponse()->getBody()->getContents();
            }else
                dd('Oops! Internet connection got wrong somewhere...');

        }catch(ConnectException $ex){
            dd('Oops! Server down or invalid request');
        }catch(Exception $ex){
            dd('Please reload page to fix');
        }
       // echo $jsonResponse;
       $this->response = $jsonResponse;
   }
   public function delete(){

       $url = $this->url;
       $data = $this->data;
       //this sends a post request to the API
       $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('DELETE', $url,
                                    ['form_params'=>
                                            $data,
                                    ['http_errors' => true]
            ]);

           $jsonResponse  = $res->getBody()->getContents();
        }catch(ClientException $ex){

            $jsonResponse = $ex->getResponse()->getBody()->getContents();
        }catch(RequestException $ex){

          dd('Oops! Internet connection got wrong somewhere...');

        }
       // echo $jsonResponse;
        $this->response = $jsonResponse;
   }

   public function put(){

       $url = $this->url;
       $data = $this->data;
       //this sends a post request to the API
       $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('PUT', $url,
                                    ['form_params'=>
                                            $data,
                                    ['http_errors' => true]
            ]);

           $jsonResponse  = $res->getBody()->getContents();
        }catch(ClientException $ex){

            $jsonResponse = $ex->getResponse()->getBody()->getContents();
        }catch(RequestException $ex){

          dd('Oops! Internet connection got wrong somewhere...');

        }
       // echo $jsonResponse;
        $this->response = $jsonResponse;
   }
}
