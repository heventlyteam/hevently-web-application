<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Session\SessionManager;
use Illuminate\Support\Facades\Input;

class SPProfileEdit extends Controller
{
    public $url = 'http://api.hevently.com/v1';
    //This  class is responsilble for anything that deals with SP's profile editing
    public $spID;
    public function accountEditLoad(){
        //this will only load the html file for editing profile
        return View('sp.profile_edit');
    }

    public function accountEditLoad2(Request $request){
        //this will handle the post request
        if($request->has('bizname') && $request->has('about') && $request->has('address')){
            $data[]= '';
            if($request->has('longitude') && $request->has('latitude')){
                //now post to the api if found valid
                $jsonData = array('');
                $postToApi = new PostToApi($this->url, $jsonData);
            }else{
                $data['error'] = 'Invalid address.';
                $data['inputs'] = $request;
            }

        }else{
            $data = array('error'=>'no field can be left empty','inputs'=>$request);
        }

        return View('sp.profile_edit')->withData($data);
    }
    public function displayPics(){
        //this only displays pictures that have been prev uploaded
        //if otherwise then give room for adding new/replacing existing
        //get the complete info about the current user

        $sessionInfo = new \Session();
        $userInfo = $sessionInfo::get('sp_info');
        $link = $userInfo->link;
        $data = $this->loadProviderInfo($link);
        return View('sp.viewdp')->withData($data);
    }

    public function uploadPic(Request $request){
        //this will upload the images to the server
        //if the post request is the main image
        $sessionInfo = new \Illuminate\Support\Facades\Session();
        $userInfo = $sessionInfo::get('sp_info');
        $provider_id = $userInfo->user_id;
        if($request->hasFile('background')){
            $destinationPath = 'uploads'; // upload path
            $extension = Input::file('background')->getClientOriginalExtension(); // getting image extension
            $fileName = rand(11111,99999).''.time().'.'.$extension; // renameing image
            Input::file('background')->move($destinationPath, $fileName);
            $pathToFile = 'C:/xampp2/htdocs/cavve.com/cavve/public/'.$destinationPath.'/'.$fileName;
            $file = fopen(''.$destinationPath.'/'.$fileName.'','r');
            //$data = array('provider_id'=>$provider_id);
            $data = [
                'multipart' => array(
                    array(
                        'name'     => 'provider_id',
                        'contents' => ''.$provider_id.''
                    ),
                    array(
                        'name'     => 'background',
                        'contents' => fopen(''.$destinationPath.'/'.$fileName.'', 'r')
                    )
                )
            ];
            $post = new PostToApi($this->url.'/provider/profile/image', $data);
            $post->post(''.$destinationPath.'/'.$fileName.'');
            $respose = $post->response;
            $response2 =json_decode($respose, true);

            if(isset($response2['status']) && $response2['status'] == 'success'){
                //it was successful
                $return2 = array('success'=>true, 'message'=>'image has been successfully uploaded');
            }else{
                $return2 = array('success'=>false, 'message'=>$response2['message']);
            }

        }
        if($request->hasFile('image')){
                $destinationPath = 'uploads'; // upload path
                $extension = Input::file('image')->getClientOriginalExtension(); // getting image extension
                $fileName = rand(11111,99999).''.time().'.'.$extension; // renameing image
                Input::file('image')->move($destinationPath, $fileName);
                $pathToFile = 'C:/xampp2/htdocs/cavve.com/cavve/public/'.$destinationPath.'/'.$fileName;
                $file = fopen(''.$destinationPath.'/'.$fileName.'','r');
                //$data = array('provider_id'=>$provider_id);
                $data = [
                    'multipart' => array(
                        array(
                            'name'     => 'provider_id',
                            'contents' => ''.$provider_id.''
                        ),
                        array(
                            'name'     => 'image',
                            'contents' => fopen(''.$destinationPath.'/'.$fileName.'', 'r')
                        )
                    )
                ];
                $post = new PostToApi($this->url.'/provider/profile/image', $data);
                $post->post(''.$destinationPath.'/'.$fileName.'');
                $respose = $post->response;
                $response2 =json_decode($respose, true);

                if(isset($response2['status']) && $response2['status'] == 'success'){
                    //it was successful
                    $return2 = array('success'=>true, 'message'=>'image has been successfully uploaded');
                }else{
                    $return2 = array('success'=>false, 'message'=>$response2['message']);
                }



        }

        if(isset($return1) && isset($return2)){
            $error = true;
            if(!$return1['success'] && $return2['success']){
                $msg = "Background image could not be set but thumbnail image was successfully uploaded";
                $error = false;
            }

            if($return1['success'] && !$return2['success']){
                $msg = "Background image has been set but thumbnail image was not uploaded";
                $error = false;
            }

            if($error){
                //then return a success message
                $return = array('success'=>true, 'message'=>'Image has been successully set');
            }else{
                $return = array('success'=>false, 'message'=>$msg);
            }
        }else if(isset($return1) || isset($return2)){
            if((isset($return1) && !$return1['success']) || (isset($return2) && !$return2['success'])){
                $return = isset($return1) ? $return1 : $return2;
            }else
                $return = array('success'=>false, 'message'=>'Image has successfully been uploaded');
        }else
            $return = array('success'=>false, 'message'=>'No image uploaded');

        //return json_encode($return, true);
        //session()->flash('status', json_encode($return, true));
        $session = new \Session();
                    $session::put('status', json_encode($return, true));
        return redirect('account');
    }
    public function changePassword(Request $request){
        //this is to change the password of the provider
        if($request->method() == 'POST'){

            if($request->has('new_password1') && $request->has('new_password2') && $request->has('old_password')){
                $new_password1 = $request->get('new_password1');
                $new_password2 = $request->get('new_password2');
                $old_password = $request->get('old_password');
                $sessionInfo = new \Illuminate\Support\Facades\Session();
                $userInfo = $sessionInfo::get('sp_info');
                $provider_id = $userInfo->user_id;

                if($new_password1 != $new_password2)
                    $data = array("error"=>"Your new passwords do not match");
                if(cowunt($new_password1) > 7)
                    $data = array("error"=>"Your password cannot contain less than 7 characters");
                if(isset($data) && count($data) > 0)
                    return View('sp.pwdcng')->withData(json_encode($data, true));
                unset($data);
                $data = array('password'=>$new_password1,'rpassword'=>$new_password2, 'opassword'=>$old_password, 'usernanme'=>$provider_id);
                var_dumpt($data);
                    $postTo = new PostToApi($this->url.'/provider/update', $data);
                $postTo->post();
                $response = $postTo->response;
                $res = json_decode($response, true);
                unset($data);
                if($res['status'] == 'success')
                    $data = array('status'=>true, 'message'=>'Password has successfully been changed');
                else
                    $data = array('status'=>false, 'message'=>isset($res['message']) ? $res['message'] : '' );

                return View('sp.pwdcng')->withData($data);

            }else{
                $data = array("error"=>"You can't leave any of the fields blank");
                return View('sp.pwdcng')->withData($data);
            }
        }else{
            //this means that the request is just to load the front end
            return View('sp.pwdcng');
        }
    }
    public function loadProfile($provider = null){
        //this is to load the profile of sp
        //it sends a post erquest to the api to get the info of the provider
        if(isset($provider)){
            $dataToSend = array();
            $postTo = new PostToApi($this->url.'/provider/'.$provider, $dataToSend);
            $postTo->get();
            $response = $postTo->response;
            $data = json_decode($response, true);
            if($data['status'] == 'success'){
                return View('sp.profile')->withData($data);
            }else{
                $data= 'The profile of the service provider you request for does not exist or must have been deleted';
                return View('reg.error')->withData($data);
            }
            return View('sp.profile')->withData($data);
        }
        $response = $this->SPProfileEdit()->loadProviderInfo();
        $response2 = json_decode($response, true);
        if($response2['status'] == 'success') {
            $data = $response2;
            return View('sp.profile')->withData($data);
        }else {
            $data= 'The profile of the service provider you request for does not exist or must have been deleted';
            return View('reg.error')->withData($data);
        }
    }
    public function loadProviderInfo($link){
        //the link must have been provided
        $dataToSend = array();
        $postTo = new PostToApi($this->url.'/provider/'.$link, $dataToSend);
        $postTo->get();
        $response = $postTo->response;
        return $response;
    }
    public function loadAccountSettings(){
        return View('sp.settings');
    }

}
