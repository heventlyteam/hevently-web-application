<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class SPReview extends Controller
{
    //this is to add and view reviews
    public $url = 'http://api.hevently.com/v1';
    public function addReview(Request $request){
        //this is to add reviews
        $spLogin = new \App\Http\Controllers\LoginSessionManagement();
        $sessionInfo = new \Illuminate\Support\Facades\Session();
        $userInfo = $sessionInfo::get('userinfo');
        if(($request->has('name') || $spLogin::validateLoginSession('userinfo')) &&
            ($request->has('email') || $spLogin::validateLoginSession('userinfo')) &&
            $request->has('service') && $request->has('attendance') && $request->has('comment') &&
            $request->has('provider') && $request->has('punctuality')  && $request->has('quality')
        ){
            $service = $request->get('service');
            $attendance = $request->get('attendance');
            $punctuality = $request->get('punctuality');
            $quality =$request->get('quality');
            $comment = $request->get('comment');
            $provider = $request->get('provider');
            $name = ($spLogin::validateLoginSession('userinfo')) ? $userInfo->username : $request->get('name');
            $email = ($spLogin::validateLoginSession('email')) ? $userInfo->email : $request->get('email');
            
            $postTo = new PostToApi($this->url.'/provider/review', array('attendance'=>$attendance, 'service'=>$service,'provider_id'=>$provider,'name'=>$name, 'email'=>$email, 'comment'=>$comment, 'punctuality'=>$punctuality, 'quality'=>$quality));
                        $postTo->post();
            $response = $postTo->response;
            $responseEn = json_decode($response, true);
            if($responseEn['status'] == 'success'){
                $data = array('status'=>true, 'message'=>'Review has successfully been added');
            }else{
                $data = array('status'=>false, 'message'=>'Review was not aded because: '.$responseEn['message']);
            }
            return json_encode($data, true);
        }else{
            $data = array('status'=>false, 'message'=>'One of the fields is found blank, if false then reload this page and try again');
            return json_encode($data, true);
        }
    }
    
    public function getreviews($provider){
        //provider id expected
       $postTo = new PostToApi($this->url.'/provider/reviews/'.$provider, array(''));
                        $postTo->get();
       $response = $postTo->response;
        return $response;
    }

    public function giveHeart($provider)
    {
        $url = $this->url . '/provider/love';
        //this assumes that the user is logged in
        $sessionInfo = new \Illuminate\Support\Facades\Session();
        if ($sessionInfo::has('userinfo')){
        $userInfo = $sessionInfo::get('userinfo');
        $user_id = $userInfo->user_id;
        //echo ' '.$provider;
        $data = array('provider_id' => $provider, 'user_id' => $user_id);
        $post = new PostToApi($url, $data);
        $post->post();
        $response = $post->response;
            $res = json_decode($response, true);
            if($res['status'] == 'success'){
                if($res['data']['ok'] == '1' && $res['data']['nModified'] == '1'){
                    $array = array('status'=>true, 'message'=>'Youve successfully liked this provider');
                    echo json_encode($array, true);
                }else{
                    $array = array('status'=>true, 'message'=>'youve successfully liked this provider');
                    echo json_encode($array, true);
                }
            }else{
                $array = array('status'=>false, 'message'=>'Connection problem');
                echo json_encode($array, true);
            }
        }else{
            $array = array('status'=>false, 'message'=>'You have to be logged in before you can like this provider');
            echo json_encode($array, true);
        }
    }
}
