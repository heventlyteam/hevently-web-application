<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class SearchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $url = 'http://api.hevently.com/v1/';
     public function search(Request $request){
         //this function will search for service providers and will return it to the listing.blade.php1
         $data = array('');
         if($request->has('address') && $request->has('longitude') && $request->has('latitude') && $request->has('category')){
            //send request to the server
             $url = 'http://api.hevently.com/v1/search/provider';
             $data = array('address'=>$request->address, 'longitude'=>$request->longitude, 'latitude'=>$request->latitude, 'category'=>$request->category, 'keyword'=>( $request->has('keyword')) ? $request->keyword:'');
             //unset($data);
             //$data = array('longitude'=>$request->longitude, 'latitude'=>$request->latitude);
             $sendReq = new PostToApi($url, $data);
                        $sendReq->get();
             echo $recieved = $sendReq->response;
             $jsonRec = json_decode($recieved, true);
             if(isset($jsonRec['error'])) {
                 $data['success'] = false;
                 $data['data'] = $jsonRec['error'];
                 $data['inputs'] = $request;
             }else{
                 unset($data);
                 $data = $jsonRec;
             }
             //process ths later
         }else{

             $listAllSP = new SPController();
             $getItems = $listAllSP->listAll();
             $jsonRec = json_decode($getItems, true);
             $data = $jsonRec;
         }
         return View('listing.listings')->withData($data);
     }

    public function searchJob(Request $request){
        $data = array();
        if($request->has('longitude') && $request->has('latitude') && $request->has('category')){
            $url = $this->url.'search/work';
            $toSend = array('category'=>$request->category, 'longitude'=>$request->longitude, 'latitude'=>$request->latitude);
            $postT = new PostToApi($url, $toSend);
                     $postT->get();
             $response = $postT->response;
            $jsonRes = json_decode($response, true);
            $data['success'] = true;
            $data['inputs'] = $request->all();
            $data['all_jobs'] = $jsonRes;
        }else{
            $data['success'] = false;
            $data['reason'] = 'A field was left blank';
            $data['inputs'] = $request;
            //dont forget to list all
            $listAll = new JobController();
            $data['all_jobs'] = json_decode(($response = $listAll->getAllJobs()),true);
        }

        return View('job')->withData($data);
    }
}
