<?php

namespace App\Http\Controllers;


use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use GuzzleHttp\Message\Request as Request2;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Collection;
class UserController extends Controller
{
    //the ,mainfunction of this controller is to controll the user and the service prpovider
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   public function activate(Request $request, $id, $c){
       $data = array('id'=>$id, 'c'=>$c);
       $url = 'http://api.hevently.com/v1/user/register/confirm';
       $recorver = new PostToApi($url, $data);
                   $recorver->get();
       $response = $recorver->response;
       $responceEncode = json_decode($response);
       if($responceEncode['status'] != 'success'){
               $data = $this->activationSuccess();
               return view('reg.success', compact('data'));
       }else{
              $data = (isset($responceEncode['message']) ? $responceEncode['message'] : 'Uncatchable error');
              return view('reg.error')->withData($data);
       }
   }
   public function changePassword($token=null, $source, $password){
       //the source tells this method the source of the call
       //if it is from password recorvery
       //then use token else use session
       if($source == 'pwd_rec'){
            $data = array('link'=>$token, 'password'=>$password, 'rpassword'=>$password);
            $url = 'http://api.hevently.com/v1/user/';
            $recorver = new PostToApi($url, $data);
                        $recorver->put();
            $response = $recorver->response;
            $jsonResponse = json_decode($response);
            if($jsonResponse['status']=='success'){
                $return = array('status'=>true);
            }else{
                $return = array('status'=>false, 'reason'=>isset($jsonResponse['reason'])?$jsonResponse['reason']:'Could not connect to the internet');
            }
        }
        return $return;
   }
   public function regUserSuccessfull(Request $request){
       
       $email = $request->email;
       return view('reg.success', compact('email'));
   }
   public function registerUser(Request $request){
       //send the post request to the user
       if($request->has('username') 
       && $request->has('email') 
       && $request->has('password') 
       && $request->has('rpassword')){
           $data = array('username'=>$request->username, 'password'=>$request->password, 'rpassword'=>$request->rpassword, 'email'=>$request->email);
           $url = 'http://api.hevently.com/v1/user/register';
           $post = $this->postData($data, $url);
          // echo($post);
           $getTO = json_decode($post, true);
           
           if($getTO['status'] == 'success'){
               //this impliees that the registration was successfull
              return $this->regUserSuccessfull($request);
           }else{
               //the registrartion was not successfull
               //the main function of the array below is to hold information gotten from the data
               //field to make them visible to the blade template
               
             $field = array();
               
               if(array_key_exists('data',$getTO)){
                   if(array_key_exists('rpassword',$getTO['data']))
                        $field['rpassword'] = $getTO['data']['rpassword'];
                        
                   if(array_key_exists('password',$getTO['data']))
                        $field['password'] = $getTO['data']['password'];
                        
                   if(array_key_exists('username',$getTO['data']))
                        $field['username'] = $getTO['data']['username'];
                        
               }
               $send = array();
             $data = array('sucess'=>false,'reason'=>$getTO['message'], 'fields'=>$field, 'inputs'=>$request);
             return view('register')->withData($data);
           }
       }else{
           //return error
           
             $data = array('sucess'=>false,'reason'=>'Some fields are missing','fields'=>[], 'inputs'=>$request);
             return view('register')->withData($data);
       }
   }
   
   public function registerProvider(Request $request){
       //this function is here just to register  a service provider
       //send the post request to the user
       if($request->has('category') 
       && $request->has('bizname') 
       && $request->has('password') 
       && $request->has('rpassword')
       && $request->has('about')
       && $request->has('email')
       && $request->has('address')
       && $request->has('longitude')
       && $request->has('latitude')
       && $request->has('username')){
            $data = array('username'=>$request->username, 
            'password'=>$request->password, 
            'rpassword'=>$request->rpassword, 
            'email'=>$request->email,
            'about'=>$request->about,
            'category'=>$request->category,
            'bizname'=>$request->bizname,
            'latitude'=>$request->latitude,
            'longitude'=>$request->longitude
            );
           $url = 'http://api.hevently.com/v1/provider/register';
            $post = $this->postData($data, $url);
             $get = json_decode($post, true);
           if($get['status'] == 'success'){
               //this impliees that the registration was successfull
             //show the success page
               $data = $this->passwordRecTemplate($request->email);
               return view('reg.success', compact('data'));
           }else{
               //the registrartion was not successfull
             $data = array('sucess'=>false,'reason'=>((isset($get['message'])) ? $get['message']:'Could not connect to the server'),'inputs'=>$request);
             return view('sp.register-biz')->withData($data);
           }
       }else{
           //return error
             $data = array('sucess'=>false,'reason'=>'Some fields are missing/left blank','inputs'=>$request);
             return view('sp.register-biz')->withData($data);
       }
   }
   public function postData($data, $url){
       //this function will post $data to the $url
                 
        $client = new \GuzzleHttp\Client();
        try{
            $res = $client->request('POST', $url,
                                    ['form_params'=>
                                            $data,
                                    ['http_errors' => true]
            ]);
           
           $jsonResponse  = $res->getBody()->getContents();
        }catch(ClientException $ex){
        
            $jsonResponse = $ex->getResponse()->getBody()->getContents();
        }catch(RequestException $ex){

          dd('Oops! Internet connection got wrong somewhere...');
            
        }
       // echo $jsonResponse;
        return $jsonResponse;
   }
   
   public function loginUser(Request $request){
       //if the user is logged in in any of the accounts then show adviced to log out from on account
       $checkLogin = new LoginSessionManagement();
       if($checkLogin->validateLoginSession('sp_info')){
           $data = '<p>You are already logged in as a service provider, therefore, you may need to be <a href="provider/logout">logged out</a> before you can <a href="login">login</a> as a user</p>';
           return View('reg.error')->withData($data);
       }

       if($checkLogin->validateLoginSession('userinfo')){
          return redirect('');
       }

       //to log a user in
        if($request->has('email') && $request->has('password')){
            //send apost request to the user
           $data = array('email'=>$request->email,'password'=>$request->password);
           $url = 'http://api.hevently.com/v1/user/login';
            $post = $this->postData($data, $url);
          // echo($post);
            $getTO = json_decode($post, true);
           if($getTO['status'] == 'success'){
               //this means that the login was successfull
               $id = $getTO['data']['_id']['$id'];
               $username = $getTO['data']['username'];
               $email  = $getTO['data']['email'];
               $password  = ''; //$getTO['data']['password'];
               $request_at  = ''; //$getTO['data']['created_at']['sec'];
               //$confirm = $getTO['data']['confirm'];
               $confirm = ''; //$getTO['data']['status'];
               $key = str_random(15);
               $status = ''; //$getTO['data']['status'];
               //now send this to the auth to allow login 
              //call the controller for the session management
              
              $login = new LoginSessionManagement();
              $dataTOSend = array('login_key'=>$key,'id'=>$id,'email'=>$email,'request_at'=>$request_at, 'confirm'=>$confirm, 'status'=>$status,'username'=>$username);
              $login->setAll($dataTOSend);
              $login->setSessionName('userinfo');
              $login->createSession();
              //now that the session has been successfully created then redirect the user to the homepage
              
              return redirect('account');
           }else{ 
             $data = array('sucess'=>false,'reason'=>$getTO['message'],'fields'=>[], 'inputs'=>$request);
             return view('login')->withData($data);
           }
        }else{
             $data = array('sucess'=>false,'reason'=>'Some field(s) are missing or left blank','fields'=>[], 'inputs'=>$request);
             return view('login')->withData($data);
        }

   }
   
     public function loginProvider(Request $request)
     {
         $checkLogin = new LoginSessionManagement();
         if($checkLogin->validateLoginSession('userinfo')){
             $data = '<p>You are already logged in as a user, therefore, you may need to be <a href="logout">logged out</a> before you can <a href="provider/login">login</a> as a user</p>';
             return View('reg.error')->withData($data);
         }

         if($checkLogin->validateLoginSession('sp_info')){
             return redirect('');
         }

         //to log a user in
             //to log a user in
             if($request->has('email') && $request->has('password')){
                 //send apost request to the user
                 $data = array('email'=>$request->email,'password'=>$request->password);
                 $url = 'http://api.hevently.com/v1/provider/login';
                  $post = $this->postData($data, $url);
                 // echo($post);
                 $getTO = json_decode($post, true);
                 if($getTO['status'] == 'success'){
                     //this means that the login was successfull
                     $id = $getTO['data']['_id']['$id'];
                     $username = $getTO['data']['username'];
                     $email  = $getTO['data']['email'];
                     $link  = $getTO['data']['link'];
                     $key = str_random(15);
                     //now send this to the auth to allow login
                     //call the controller for the session management

                     $login = new LoginSessionManagement();
                     $dataTOSend = array('login_key'=>$key,'id'=>$id,'email'=>$email,'username'=>$username, 'link'=>$link);
                     $login->setAll($dataTOSend, 'sp_info');
                     $login->setSessionName('sp_info');
                     $login->createSession();
                     //now that the     session has been successfully created then redirect the user to the homepage

                     return redirect('/');
                 }else{
                     $data = array('sucess'=>false,'reason'=>isset($getTO['message'])?$getTO['message'] : 'Server could not proccess data','fields'=>[], 'inputs'=>$request);
                     return view('sp/login')->withData($data);
                 }
             }else{
                 $data = array('sucess'=>false,'reason'=>'Some field(s) are missing or left blank','fields'=>[], 'inputs'=>$request);
                 return view('sp/login')->withData($data);
             }



     }
      public function activationSuccess()
         {
             return '

    <div class="main">
        <div class="main-inner">
            <div class="container">
                <div class="content">



                    <div class="row">
    <div class="col-sm-4 col-sm-offset-4" style="background:#fefefe;padding:2% ">
        	<div align="center">
				<div class="container-fluid bg-1 text-center">
  					<img src="right.jpg" class="" style="display: inline; width: 40px; height: 40px;" alt="Bird">
                      <h2> <span class="text-success">Account Activated</span></h2>
 					 <img src="success1.jpg" class="img-circle" alt="Bird">
                      <h3><span class="text-success">Account Activated!</span></h3>
 					 <h4><span class="text-info">Activation link has been found valid..</span></h4>
 					 <h4><span class="text-info">Account has been activated successfuly..</span></h4>
 					 <h4><span class="text-info"><a href="login">Click here to login</a>..</span></h4>
				</div>
			</div>
    </div><!-- /.col-sm-4 -->
</div><!-- /.row -->

                </div><!-- /.content -->
            </div><!-- /.container -->
        </div><!-- /.main-inner -->
    </div><!-- /.main -->';

         }

   public function passwordRecTemplate($email = null){
       return '
    <div class="main">
        <div class="main-inner">
            <div class="container">
                <div class="content">
                    


                    <div class="row">
    <div class="col-sm-4 col-sm-offset-4" style="background:#fefefe;padding:2% ">
        	<div align="center">
				<div class="container-fluid bg-1 text-center">
  					<img src="right.jpg" class="" style="display: inline; width: 40px; height: 40px;" alt="Bird">
                      <h2> <span class="text-success">Registration Successful</span></h2>
 					 <img src="success1.jpg" class="img-circle" alt="Bird">  
                      <h3><span class="text-danger">Account INACTIVE!</span></h3>
 					 <h4><span class="text-info">Activation link has been sent to '.$email.', please click on the activation link to confirm your account...</span></h4>
 					</div>
			</div>
    </div><!-- /.col-sm-4 -->
</div><!-- /.row -->

                </div><!-- /.content -->
            </div><!-- /.container -->
        </div><!-- /.main-inner -->
    </div><!-- /.main -->';
   }

    public function logout(){
        session()->forget('userinfo');
        return redirect('');
    }
    public function logoutSp(){
        session()->forget('sp_info');
        return redirect('');
    }
}
