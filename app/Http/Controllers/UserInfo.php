<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class UserInfo extends Controller
{
    private $login_key;
    public $user_id;
    public $username;
    public $email;
    public $created_at;
    public $confirm;
    public $activated;
    
    public function setAll($json){
        $request = $json;
        $this->login_key = $request['login_key'];
        $this->user_id = $request['id'];
        $this->email = $request['email'];
        $this->created_at = $request['request_at'];
        $this->confirm = $request['confirm'];
        $this->activated = $request['status'];
        $this->username = $request['username'];
    }
    
}
