<?php

namespace App\Http\Middleware;

use Closure;

class CheckLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
     private $is_log;
    public function __construct(){
        
        $c = new \App\Http\Controllers\LoginSessionManagement();
        $c->setSessionName('userinfo');
       $this->is_log =  $c::validateLoginSession('userinfo');
    }
    public function handle($request, Closure $next)
    {
        if(!$this->is_log)
        return  redirect('login');
        return $next($request);
    }
}
