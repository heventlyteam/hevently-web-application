<?php

namespace App\Http\Middleware;

use Closure;

class CheckSPLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    private $is_log;
    public function __construct(){

        $c = new \App\Http\Controllers\LoginSessionManagement();
        $c->setSessionName('sp_info');
        $this->is_log =  $c::validateLoginSession('sp_info');
    }
    public function handle($request, Closure $next)
    {
        if(!$this->is_log)
            return  redirect('provider/login');
        return $next($request);
    }
}
