<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
use GuzzleHttp\Message\Request;

use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\Request as Request2;
Route::get('/', function () {
    return view('index');
});
Route::get('index', function(){
    return Redirect('');
});

Route::get('provider/register', function(){
   return View('sp/register-biz');
});
Route::post('provider/register', 'UserController@registerProvider');
Route::get('provider/login', function(){
   $spLogin = new \App\Http\Controllers\LoginSessionManagement();
   if($spLogin::validateLoginSession('sp_info'))
      return redirect('/');
   return View('sp/login');
});
Route::post('provider/login', 'UserController@loginProvider');
Route::get('serviceprovider/login', function(){
   return redirect('provider/login');
});
Route::get('serviceprovider/register', function(){
   return redirect('provider/register');
});
Route::get('/listings', 'SearchController@search');
Route::get('listing-detail/{$provider}', 'SPProfileEdit@loadProfile');
Route::get('login', function(){

   $spLogin = new \App\Http\Controllers\LoginSessionManagement();
   if($spLogin::validateLoginSession('userinfo'))
      return redirect('/');
   return View('login'); 
});
Route::get('register', function(){
   return View('register'); 
});
Route::post('register', 'UserController@registerUser');
Route::get('about', function(){
   return View('about'); 
});
Route::get('terms-conditions', function(){
   return Redirect('terms-conditions.html');
});
Route::get('faq', function(){
   return View('faq'); 
});
Route::get('contact', function(){
   return View('contact'); 
});
Route::post('login','UserController@loginUser');
Route::get('user/dashboard', function(){
   return View('user.dashboard'); 
});
Route::get('user/profile', function(){
   return View('user.profile');
});
Route::get('user/settings', function(){
   return View('user.settings');
});
Route::get('jobber', function(){
   return redirect('jobs');
});
//works/
Route::get('jobs', 'SearchController@searchJob');
/*
//all the routes within the account
*/

Route::get('sp/settings', 'SPProfileEdit@loadAccountSettings');
Route::group(['middleware'=>'authSP'], function(){

    /**
     * The dashboard ajax functions
     * starts
     */
    Route::get('edit_profile', 'SPProfileEdit@accountEditLoad');
    Route::post('edit_profile', 'SPProfileEdit@accountEditLoad2');
    Route::get('sp/cngpassword', 'SPProfileEdit@changePassword');

    Route::get('jobs/apply/', 'JobController@apply');
    /**
     * ends here
     */
 
    Route::post('sp/upload', 'SPProfileEdit@uploadPic');
    Route::get('sp/view/dp', 'SPProfileEdit@displayPics');
    Route::get('sp/profile', 'SPProfileEdit@loadProfile');
    Route::get('sp/settings', 'SPProfileEdit@loadAccountSettings');
    Route::get('account', function(){

        //check if the user/provider session exists
        $check = new \App\Http\Controllers\LoginSessionManagement();
        $checkUser = $check->validateLoginSession('userinfo');
        $checkProvider = $check->validateLoginSession('sp_info');
        $sessionInfo = new \Illuminate\Support\Facades\Session();
        $userInfo = $sessionInfo::get('sp_info');
        $infoFom['userinfo'] = $userInfo;
        //now get more info from the SPcontroller
        $loadI = new \App\Http\Controllers\SPProfileEdit();
        $providerInfo = $loadI->loadProviderInfo($userInfo->link);
        $infoFom['provider'] = json_decode($providerInfo, true);
        return View('user.dashboard', compact('infoFom'));
});
    Route::post('provider/password/change', 'SPProfileEdit@changePassword');

});
/*
//////////////
*/

/* routes that can only be accessed by logged user
*/

Route::group(['middleware'=>'authUser'], function() {
    Route::get('jobs/post', 'JobController@postJob');
    Route::post('jobs/post', 'JobController@postJob');
});

/*
*/
Route::get('test', function(){
    //test google map
    return view('test');
});

Route::get('logout', 'UserController@logout');
Route::get('provider/logout', 'UserController@logoutSp');
Route::get('sp/provider/logout', 'UserController@logoutSp');
/**
*Recovery/activatin link re-send
**/

Route::get('pasword/recovery', 'AccountController@recorverPassword');
Route::post('pasword/recovery', 'AccountController@recorverPassword');
Route::get('pasword/recovery/confirm/{link}', 'AccountController@recorverPasswordConfirm');
Route::post('pasword/recovery/confirm/{link}', 'AccountController@recorverPasswordConfirm');
/**
*all
**/

/**
Account activation

**/
Route::get('account/confirm/{id}/{c}', 'UserController@activate');
/**
account activation end
**/
Route::get('services.php', function(){
   return View('services');
});

Route::get('loadjobid', function(Request2 $request){
    $getJ = new \App\Http\Controllers\JobController();
    $job = $getJ->getJob($request);
    $jobs = json_decode($job, true);
    $jobs = $jobs['data'];
    //now we convert to html
    $time =  $jobs['created_at']['sec'];
    //convert useing timestamp
    $d = date('d',$time);
    $y = date('y',$time);
    $m = date('m',$time);
    $date = $d.'/'.$m.'/'.$y;

    $bud = $jobs['job']['budget'];
    $exBud = explode(' ', $bud);
    $minBud= $exBud[0];
    $maxBud = (isset($exBud[1])) ? $exBud[1] : '';
   echo $html = '<div>
                Added By: '.$jobs['username'].'
                    <br />
                Event Type: '.$jobs['job']['event_type'].'
                <br />
                Added On: '.$date.'
                <br />
                Job Details / Skills required: '.$jobs['job']['description'].'
                <br />
                Profession Required: '.$jobs['job']['title'].'
                <br />
                Budjet: '.$minBud.' - '.$maxBud.'
                <br />
                Deadline:
                <br />
                <a class="button button-default" href="jobs/apply?job_id='.$jobs['link'].'">Apply</a>
            </div>';
});
Route::post('sp/review/','SPReview@addReview');
Route::get('sp/heart/{provider}','SPReview@giveHeart');
Route::get('provider/{provider}/hire', function($provider){
    //check if provider exist
    $loadProvider = new \App\Http\Controllers\SPProfileEdit();
    $profileInfo2 = $loadProvider->loadProviderInfo($provider);
    $profileInfo = json_decode($profileInfo2, true);
    return View('hire', compact('profileInfo'));
});
Route::post('provider/{provider}/hire', 'HireProvider@hire');
Route::get('provider/{provider}', 'SPProfileEdit@loadProfile');
Route::get('test3', function(){
   return View('test');
});
Route::post('test3', function(Request $request){
   if($request->hasFile('image'))
       echo 'yes';
    else
        echo 'no';
});