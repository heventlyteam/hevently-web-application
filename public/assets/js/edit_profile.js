//this is the script to load the user profile
$('.container-fluid .main_row2').hide(0);
$('.alert').hide(0);
$('form.upload_image').submit(function(e){
    e.preventDefault();
    //alert();
    upload_image();
});
$('input.upload_image_').click(function(e){
    e.preventDefault();
    //alert();
    upload_image();
});
function loadDashboard(){

    $('.container-fluid .main_row').show(0);
    $('.container-fluid .main_row2').hide(0);
}
function upload_image(form){
    //alert();
    //get all the files posted
    var token = $('input[name="_token"]').val();
    var image = $('input[name="image"]').val();
    var background = $('input[name="background"]').get(0).files[0];
    var formData = new FormData();
    
    var data = '_token='+token+''; //,
    if(image != undefined)
        formData.append('image',$('input[name="image"]').get(0).files[0]); //,
    if(background != undefined)
        formData.append('background',$('input[name="background"]').get(0).files[0]); //,

    $.ajax(
        {
            url:"sp/upload",
            type:"POST",
            cache: false,
            data: formData,
            beforeSend: function(){
                showLoading(true);
            },
            success: function(data){
                showLoading(false);
                $('.container-fluid .main_row2').html(data);
                $('.container-fluid .main_row2').show(0);
                $('.container-fluid .main_row').hide(0);
            },
            error: function(){
                showLoading(false);
                displayError("Could not connect to the internet", false);
    }

        }
    );
}
function load_edit_profile() {
    //load the ajax request scrip
$.ajax({
        url:"edit_profile",
        type:"GET",
        cache: false,
        data: '',
        beforeSend: function(){
            showLoading(true);
        },
        success: function(data){
            showLoading(false);
            $('.container-fluid .main_row2').html(data);
            $('.container-fluid .main_row2').show(0);
            $('.container-fluid .main_row').hide(0);
        },
        error: function(){
            showLoading(false);
            displayError("Could not connect to the internet", false);
        }

    });
}

function loadPasswordCng(){
    $.ajax({
        url:"sp/cngpassword",
        type:"GET",
        cache: false,
        data: '',
        beforeSend: function(){
            showLoading(true);
        },
        success: function(data){
            showLoading(false);
            $('.container-fluid .main_row2').html(data);
            $('.container-fluid .main_row2').show(0);
            $('.container-fluid .main_row').hide(0);
        },
        error: function(){
            showLoading(false);
            displayError("Could not connect to the internet", false);
        }

    });
    return false;
}

//provider/password/change


function changePassword(){
    var old_pwd = $('input[name="old_password"]').val();
    var new_pwd1 = $('input[name="new_password1"]').val();
    var new_pwd2 = $('input[name="new_password2"]').val();
    var token = $('input[name="_token"]').val();
    //alert('new_password1='+new_pwd1+"&new_password2="+new_pwd2+"&old_password="+old_pwd+"_token="+token);
    $.ajax({
        url:"provider/password/change",
        type:"POST",
        cache: false,
        data: 'new_password1='+new_pwd1+"&new_password2="+new_pwd2+"&old_password="+old_pwd+"&_token="+token,
        beforeSend: function(){
            showLoading(true);
        },
        success: function(data){
            showLoading(false);
            $('.container-fluid .main_row2').html(data);
            $('.container-fluid .main_row2').show(0);
            $('.container-fluid .main_row').hide(0);
        },
        error: function(){
            showLoading(false);
            displayError("Could not connect to the internet", false);
        }

    });
    return false;
}
function loadViewDP(){
    $.ajax({
        url:"sp/view/dp",
        type:"GET",
        cache: false,
        data: '',
        beforeSend: function(){
            showLoading(true);
        },
        success: function(data){
            showLoading(false);
            $('.container-fluid .main_row2').html(data);
            $('.container-fluid .main_row2').show(0);
            $('.container-fluid .main_row').hide(0);
        },
        error: function(){
            showLoading(false);
            displayError("Could not connect to the internet", false);
        }

    });
    return false;
}
function showLoading(show_){
    if(!show_) {
        $('.loader').hide(0);
    }else{
        $('.loader').show(0);
    }

}

function displayError(content, situation){
    content = '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><br/>'+content;
    if(situation){
        var className = 'alert-success';
    }else{
        var className = 'alert-danger';
    }
    $('.alert').show(0);
    $('.alert').addClass(className);
    $('.alert').html(content);
    setTimeout(function(){
        $('.alert').hide(100);
    }, 5000);
}
function editProfile(){
    //get the content of the form
    var username = $('input[name="username"]').val();
    var biznmae = $('input[name="bizname"]').val();
    var about_biz = $('input[name="about_biz"]').val();
    var address = $('input[name="address"]').val();
    var longitude = $('input[name="longitude"]').val();
    var latitude = $('input[name="latitude"]').val();
    var video = $('input[name="video_link"]').val();
    var fb_link = $('input[name="fb_link"]').val();
    var biz_web = $('input[name="personal_web"]').val();
    var p_num = $('input[name="p_num"]').val();
    var token = $('input[name="_token"]').val();

    var data = 'username='+usernmae+'&bizname='+bizname+'about_biz='+about_biz+'&address='+address+'&longitude='+longitude+'&latitude='+latitude+'&video='+video+'&fb_link='+fb_link+'&biz_web='+biz_web+'&p_num='+p_num+'&token='+token;

    //now if submitted
    if(p_num == '' && about_biz == '' && address == '' && token == ''){
        displayError('No field can be left blank', false);
    }else{
        //now post to edit_profile
       $.ajax({
            url:"edit_profile",
            type:"POST",
            cache: false,
            data: data,
            beforeSend: function(){
                showLoading(true);
            },
            success: function(data){
                showLoading(false);
                $('.container-fluid .main_row2').html(data);
                $('.container-fluid .main_row2').show(0);
                $('.container-fluid .main_row').hide(0);
            },
            error: function(){
                showLoading(false);
                displayError("Could not connect to the internet", false);
            }

        });
    }
    return false;
}
function loadProfile(){
    $.ajax({
        url:"sp/profile",
        type:"GET",
        cache: false,
        data: '',
        beforeSend: function(){
            showLoading(true);
        },
        success: function(data){
            showLoading(false);
            $('.container-fluid .main_row2').html(data);
            $('.container-fluid .main_row2').show(0);
            $('.container-fluid .main_row').hide(0);
        },
        error: function(){
            showLoading(false);
            displayError("Could not connect to the internet", false);
        }

    });
return false;
}