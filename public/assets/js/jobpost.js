/**
 * Created by Orisatoberu on 4/6/2016.
 */
//first of all hide set the text box from step 2-6 as

function hide_all(){
    //hide step 2
    $("#select").attr('disabled', true);
    $(".step2 button[data-id='select']").parents('.step2').find('.selectpicker').attr('disabled').selectpicker('refresh');

}
$( '#step_1_pro_needed').change(function(){
    //check if it is valid and if otherwise disable all the other divs until the proper field is selected

    if(step_1_is_valid()){
        //then enable step 2

        if(step_2_is_valid()) {
            //then enable step 2
            if(step_3_is_valid()) {
                if(step_4_is_valid()){
                    if(step_5_is_valid()){
                        step6Show(true);
                    }
                    step5Show(true);
                }
                step4Show(true);
                step3Show(true);
                step2Show(true);
            }else{
                step3Show(true);
            }
        }else{
            step2Show(true);
        }
    }else{
        //disable step 2 and the rest
        step4Show(false);
        step3Show(false);
        step2Show(false);
    }
});

$( 'select[name="work_about"]').change(function(){
    //check if it is valid and if otherwise disable all the other divs until the proper field is selected
    if(step_2_is_valid()){
        //then enable step 2if(step_2_is_valid()) {
        //then enable step 2
        if(step_3_is_valid()) {
            if(step_4_is_valid()){
                if(step_5_is_valid()){
                    step6Show(true);
                }
                step5Show(true);
            }
            step4Show(true);
            step3Show(true);
        }else{
            step3Show(true);
        }
    }else{  //disable step 2 and the rest
        step4Show(false);
        step3Show(false);
    }
});


$( '.step3 input[name="address"]').change(function(){
    //check if it is valid and if otherwise disable all the other divs until the proper field is selected
    if(step_2_is_valid()){
        //then enable step 2if(step_2_is_valid()) {
        //then enable step 2
        if(step_3_is_valid()) {
            if(step_4_is_valid()){
                if(step_5_is_valid()){
                    step6Show(true);
                }
                step5Show(true);
            }
            step4Show(true);
            step3Show(true);
        }else{
            step3Show(true);
        }
    }else{  //disable step 2 and the rest
        step4Show(false);
        step3Show(false);
    }
});

//now to step 3
$('.step3 button').click(function(){
    //check if the 3 is valid
    if(step_3_is_valid()){
        if(step_4_is_valid()){
            if(step_5_is_valid()){
                step6Show(true);
            }
            step5Show(true);
        }
        step4Show(true);
    }else{
        step4Show(false);
    }
    return false;
});
//now to step 4
$('.step4 input[name="min_budget"]').change(function(){

    if(step_4_is_valid()){
        if(step_5_is_valid()){
            step6Show(true);
        }
        step5Show(true);
    }else{
        step5Show(false);
    }
});
$('.step4 input[name="max_budget"]').change(function(){

    if(step_4_is_valid()){
        if(step_5_is_valid()){
            step6Show(true);
        }
        step5Show(true);
    }else{
        step5Show(false);
    }
});


$('.step5 input[name="deadline"]').change(function(){

    if(step_5_is_valid()){
        //alert('step 6');
        step6Show(true);
    }else{
        step6Show(false);
    }
});

function step_1_inputs(){
    this.event_type = $('select[name=work_spec]').val();
    this.professional_needed = $('select[name=profession_needed]').val();
}
function step_1_is_valid(){
    //this function is to get all the inputs of the step 1 and check if they are valid.
    //the fields are only valid if the event_type == event and the professional needed != ''
    var getInputs = new step_1_inputs();
    if(getInputs.event_type == 'Event' && getInputs.professional_needed != '')
        return true;
    else
        return false;
}


function step_2_inputs(){
    this.about = $('select[name=work_about]').val();
    this.address = $('.step2 input[name="address"]').val();
    //this.professional_needed = $('select[name=profession_needed]').val();
}
function step_2_is_valid(){
    //this function is to get all the inputs of the step 1 and check if they are valid.
    //the fields are only valid if the event_type == event and the professional needed != ''
    var getInputs = new step_2_inputs();
    if(getInputs.about != '' && getInputs.address != '')
        return true;
    else
        return false;
}


function step_3_inputs(){
    this.about = $('.step3 textarea').val();

    //this.professional_needed = $('select[name=profession_needed]').val();
}
function step_3_is_valid(){
    //this function is to get all the inputs of the step 1 and check if they are valid.
    //the fields are only valid if the event_type == event and the professional needed != ''
    var getInputs = new step_3_inputs();
    if(getInputs.about.length > 12){
        return true;
    }else{
        return false;
    }
}


function step_4_inputs(){
    this.min_budget = $('.step4 input[name="min_budget"]').val();
    this.max_budget = $('.step4 input[name="max_budget"]').val();

    //this.professional_needed = $('select[name=profession_needed]').val();
}
function step_4_is_valid(){
    //this function is to get all the inputs of the step 1 and check if they are valid.
    //the fields are only valid if the event_type == event and the professional needed != ''
    var getInputs = new step_4_inputs();
    if(getInputs.min_budget > 100 && (getInputs.max_budget > getInputs.min_budget)){
        return true;
    }else{
        return false;
    }
}


function step_5_inputs(){
    this.date = $('.step5 input[name="deadline"]').val();
    //this.professional_needed = $('select[name=profession_needed]').val();
}
function step_5_is_valid(){
    //alert('called');
    //this function is to get all the inputs of the step 1 and check if they are valid.
    //the fields are only valid if the event_type == event and the professional needed != ''
    var getInputs = new step_5_inputs();
    //alert("length : "+getInputs.date.length);
    if(getInputs.date.length == 16){
        //validate
        var splited = getInputs.date.split("/");
        //alert("array length"+splited.length);
        if(splited.length != 3){
            return false;
        }
        for(var i=0; i < 3; i++) {
            //alert('preceed to for loop');
            if (i == 0) {

                //alert('preceed to i'+i);
                if (splited[i] > 0 && splited[i] <= 31) {

                } else {
                    return false;
                }
            }

            if (i == 1) {

                //alert('preceed to i'+i);
                if (splited[i] > 0 && splited[i] <= 12) {

                } else {
                    return false;
                }
            }
            if (i == 2) {
                //alert('preceed to i'+i);
                var split2 = splited[i].split(" ");
                //alert("Length of split 2"+split2.length+" year = "+split2[0]);
                if (split2[0]   > 2015 && split2[0] <= 2019) {
                    var split3 = split2[1].split(":");
                    if(!isNaN(split3[0]) && split3[0] >= 0 && split3[0] <= 24){

                    }else{

                        //alert('error'+i);
                        return false;
                    }


                    if(!isNaN(split3[1]) && split3[0] >= 0 && split3[0] <= 60){
                        return true;
                    }else{
                        return false;
                    }

                } else {
                    return false;
            }
            }

        }
        return true;
    }else{
        return false;
    }
}



function loadStep2(){
    //this is to
}
function step2Show(show_){
    //if the show is true then set the visibility to be visible and if otherwise then set the ID to be disable
    if(show_) {
        //get the Id of the div 2 and set it to visible name="address"
        $('.step2').attr("id","enable");
        $("#select").attr('disabled', false);
        $(".step2 input[name='address']").attr('disabled', false);
        $(".step2 button[data-id='select']").parents('.step2').find('.selectpicker').attr('disabled').selectpicker('refresh');

    }else {
        $('.step2').attr("id","disable");
        $("#select").attr('disabled', true);
        $(".step2 input[name='address']").attr('disabled', true);
        $(".step2 button[data-id='select']").parents('.step2').find('.selectpicker').attr('enable').selectpicker('refresh');
    }
}

function step3Show(show_){
    //if the show is true then set the visibility to be visible and if otherwise then set the ID to be disable
    if(show_) {
        //get the Id of the div 2 and set it to visible
        $('.step3').attr("id","enable");
        $(".step3 #step3_select").attr('disabled', false);
        $(".step3 button").attr('disabled', false);

    }else {
        $('.step3').attr("id","disable");
        $(".step3 #step3_select").attr('disabled', true);
        $(".step3 button").attr('disabled', true);
    }
}


function step4Show(show_){
    //if the show is true then set the visibility to be visible and if otherwise then set the ID to be disable
    if(show_) {
        //get the Id of the div 2 and set it to visible
        $(  '.step4').attr("id","enable");
        $(".step4 input").attr('disabled', false);

    }else {
        $('.step4').attr("id","disable");
        $(".step4 input").attr('disabled', true);
    }
}

function step5Show(show_){
    //if the show is true then set the visibility to be visible and if otherwise then set the ID to be disable
    if(show_) {
        //get the Id of the div 2 and set it to visible
        $('.step5').attr("id","enable");
        $(".step5 input").attr('disabled', false);
    }else {
        $('.step5').attr("id","disable");
        $(".step5 input").attr('disabled', true);
    }
}

function step6Show(show_){
    //alert(show_);
    //if the show is true then set the visibility to be visible and if otherwise then set the ID to be disable
    if(show_) {
        //get the Id of the div 2 and set it to visible
        $('.step6').attr("id","enable");
        $(".step6 button").attr('disabled', false);
    }else {
        $('.step6').attr("id","disable");
        $(".step6 button").attr('disabled', true);
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$('a.link2').click(function(e)
{
    // Special stuff to do when this link is clicked...

    // Cancel the default action
    e.preventDefault();
});
function loadJob(jobID){
    //alert(jobID);
    $.ajax({
        url:"loadjobid",
        type:"GET",
        cache: false,
        data: 'job_id='+jobID,
        beforeSend: function(){
            showLoading(true);
        },
        success: function(data){
            showLoading(false);
            //alert(data);
            $('.modal-body').html(data);
            $("#myModal").modal();
        },
        error: function(){
            ////alert('no');
            showLoading(false);
            //displayError("Could not connect to the internet", false);
        }
    });
    return false;
}



function showLoading(show_){
    if(!show_) {
        $('.loader').hide(0);
    }else{
        $('.loader').show(0);
    }

}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////