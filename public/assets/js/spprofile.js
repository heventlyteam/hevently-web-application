function hideerror() {
    $('.show_error').html('');
}
$('form.add-review').submit(function(e){
    e.preventDefault();
    addReview();
});
$('a.link2').click(function(e)
{
    // Special stuff to do when this link is clicked...

    // Cancel the default action
    e.preventDefault();
});
function giveHeart(splink){
    //send to the server
    //alert(splink);
    $.ajax({
        url: 'sp/heart/'+splink,
        data: '',
        type: 'GET',
        success: function(data){
            //alert(data);
            var obj = JSON.parse(data);
            if(obj['status']){
                $(".show_error").html("<a href='#' class='close' data-dismiss='alert' onclick='hideerror()'  aria-label='close'>&times;</a><div class='alert alert-success'><strong>Success!</strong> Successfully liked this provider</div>");
            }
            else{
                $('.show_error').html("<a href='#' class='close' data-dismiss='alert' onclick='hideerror()'  aria-label='close'>&times;</a><div class='alert alert-warning'><strong>Error!</strong> Could not like this provider because "+obj['message']+"</div>");
            }
            //alert('succes');
        },
        error: function(){
            //alert('no');
            $('.show_error').html("<a href='#' class='close' data-dismiss='alert'  onclick='hideerror()' aria-label='close'>&times;</a><div class='alert alert-danger'><strong>Fatal!</strong> Could not connect to the server</div>");
        }
    });
    $('.show_error').show();
}

function addReview(){
    var name = $('input[name="name"]').val();
    var email = $('input[name="email"]').val();
    var service = $('input[name="service"]').val();
    var attendance = $('input[name="attendance"]').val();
    var punctuality = $('input[name="punctuality"]').val();
    var quality = $('input[name="quality"]').val();
    var token = $('input[name="_token"]').val();
    var provider_id = $('input[name="provider"]').val();
    var comment = $('textarea[name="comment"]').val();
    if(name == '' || email == '' || comment == '' || quality < 1 || service < 1 || attendance < 1 || punctuality < 1){
        $('.show_error').html("<a href='#' class='close' data-dismiss='alert' onclick='hideerror()'  aria-label='close'>&times;</a><div class='alert alert-danger'><strong>Error!</strong> Non of the review fields can be left empty. please fill them appropiately </div>");
        return false;
    }
    //alert('name='+name+'&email='+email+'&service='+service+'&attendance='+attendance+'&punctuality='+punctuality+'&quality='+quality+'&comment='+comment+'&_token='+token+'&provider='+provider_id);
    $.ajax({
        url: 'sp/review',
        data: 'name='+name+'&email='+email+'&service='+service+'&attendance='+attendance+'&punctuality='+punctuality+'&quality='+quality+'&comment='+comment+'&_token='+token+'&provider='+provider_id,
        type: 'POST',
        cache: false,
        success: function(data){

            var obj = JSON.parse(data);
            //alert(data);
            if(obj['status']){
                $(".show_error").html("<a href='#' class='close' data-dismiss='alert' onclick='hideerror()' aria-label='close'>&times;</a><div class='alert alert-success'><strong>Success!</strong> Your preview has successfully been added</div>");
            }
            else{
                $('.show_error').html("<a href='#' class='close' data-dismiss='alert' onclick='hideerror()'  aria-label='close'>&times;</a><div class='alert alert-danger'><strong>Error!</strong> "+obj['message']+" </div>");
            }
        },
        error: function(){
            $('.show_error').html("<a href='#' class='close' data-dismiss='alert'  onclick='hideerror()'  aria-label='close'>&times;</a><div class='alert alert-danger'><strong>Error!</strong> Could not connect to the server </div>");

        }
    });
}