<!DOCTYPE html>
<html>


<!-- Mirrored from preview.byaviators.com/template/superlist/faq.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 21 Nov 2015 16:10:32 GMT -->
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">

    <link href="http://fonts.googleapis.com/css?family=Nunito:300,400,700" rel="stylesheet" type="text/css">
    <link href="assets/libraries/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="assets/libraries/owl.carousel/assets/owl.carousel.css" rel="stylesheet" type="text/css" >
    <link href="assets/libraries/colorbox/example1/colorbox.css" rel="stylesheet" type="text/css" >
    <link href="assets/libraries/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" type="text/css">
    <link href="assets/libraries/bootstrap-fileinput/fileinput.min.css" rel="stylesheet" type="text/css">
    <link href="assets/css/superlist.css" rel="stylesheet" type="text/css" >
    <link rel="stylesheet" type="text/css" href="assets/css/cavve.css">
    

    <link rel="shortcut icon" type="image/x-icon" href="assets/favicon.png">

    <title>Hevently - FAQ</title>
</head>


<body>

<div class="page-wrapper">
    
@include('include.user.header')


    <div class="main">
        <div class="main-inner">
            <div class="container">
                <div class="row">
                    


                    <div class="col-sm-8 col-lg-9">
                        <div class="content">
                            <div class="page-title">
    <h1>FAQ</h1>
</div><!-- /.page-title -->

<div class="faq">
    
        <div class="faq-item">
            <div class="faq-item-question">
                <h2>How to Register a service provider's account?</h2>
            </div><!-- /.faq-item-question -->

            <div class="faq-item-answer">
                <p>
                    Just click on the register link on the top of the page where you see something like 'Are you an Event Service provider? Register or Login
                    Call us on: +2348749743947' or clike <a href="serviceprovider/register">Here</a>
                </p>
            </div><!-- /.faq-item-answer -->

            <div class="faq-item-meta">
                Was this answer helpful?
                <span class="rate">
                    <a href="#">Yes</a><span class="separator">/</span><a href="#">No</a>
                </span><!-- /.rate -->
            </div><!-- /.faq-item-meta -->
        </div><!-- /.faq-item -->
    
        <div class="faq-item">
            <div class="faq-item-question">
                <h2>Can I register as a Service provider and at the same time have a customer's account?</h2>
            </div><!-- /.faq-item-question -->

            <div class="faq-item-answer">
                <p>
                    Yes, you can.
                </p>
            </div><!-- /.faq-item-answer -->

            <div class="faq-item-meta">
                Was this answer helpful?
                <span class="rate">
                    <a href="#">Yes</a><span class="separator">/</span><a href="#">No</a>
                </span><!-- /.rate -->
            </div><!-- /.faq-item-meta -->
        </div><!-- /.faq-item -->
    
        <div class="faq-item">
            <div class="faq-item-question">
                <h2>Can I log in as a customer and at the same time as a service provider?</h2>
            </div><!-- /.faq-item-question -->

            <div class="faq-item-answer">
                <p>
                    No, You can't. Hevently does not allow you to log in these two account types at the same time for some undisclosed reason.
                </p>
            </div><!-- /.faq-item-answer -->

            <div class="faq-item-meta">
                Was this answer helpful?
                <span class="rate">
                    <a href="#">Yes</a><span class="separator">/</span><a href="#">No</a>
                </span><!-- /.rate -->
            </div><!-- /.faq-item-meta -->
        </div><!-- /.faq-item -->
    
        <div class="faq-item">
            <div class="faq-item-question">
                <h2>Can I have multiple service provider's accounts?</h2>
            </div><!-- /.faq-item-question -->

            <div class="faq-item-answer">
                <p>
                    Yes, It is allowed according to the terms and conditions of Hevently for bussinesses that seems not to correlate but
                    Hevently does not approve you having multiple accounts for a particular business, in order to prevent this we've provided
                    options to ease your account usage. These options includes, Reset password which allows you to reset your account's password
                    , Change password and also Update Account Info which allows you to  update almost all information provided by you when registering.
                </p>
            </div><!-- /.faq-item-answer -->

            <div class="faq-item-meta">
                Was this answer helpful?
                <span class="rate">
                    <a href="#">Yes</a><span class="separator">/</span><a href="#">No</a>
                </span><!-- /.rate -->
            </div><!-- /.faq-item-meta -->
        </div><!-- /.faq-item -->
    
        <div class="faq-item">
            <div class="faq-item-question">
                <h2>Can I delete my account?</h2>
            </div><!-- /.faq-item-question -->

            <div class="faq-item-answer">
                <p>
                    You may not be able to delete your account but you can contact the Hevently team for this.
                </p>
            </div><!-- /.faq-item-answer -->

            <div class="faq-item-meta">
                Was this answer helpful?
                <span class="rate">
                    <a href="#">Yes</a><span class="separator">/</span><a href="#">No</a>
                </span><!-- /.rate -->
            </div><!-- /.faq-item-meta -->
        </div><!-- /.faq-item -->
    
</div><!-- /.faq -->
@if(isset($pagination))
<div class="pager">
    <ul>
        <li><a href="#">Prev</a></li>
        <li><a href="#">5</a></li>
        <li class="active"><a>6</a></li>
        <li><a href="#">7</a></li>
        <li><a href="#">Next</a></li>
    </ul>
</div>
                            <!-- /.pagination -->
@endif

                        </div><!-- /.content -->
                    </div><!-- /.col-* -->

                    <div class="col-sm-4 col-lg-3">
                    </div><!-- /.col-* -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div><!-- /.main-inner -->
    </div><!-- /.main -->

    <footer class="footer">
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <div class="" style="background-color: white; width: 220px; padding: 10px 20px; border-radius: 5px; margin-top: 10px;">
                        <img src="assets/img/logo.png">
                    </div>
                </div><!-- /.col-* -->

                <div class="col-sm-4">
                    <h2>Contact Information</h2>

                    <p>
                        Osun-state, Nigeria<br>
                        +234-874-974-394-7,<br> <a href="#">support@hevently.com</a>
                    </p>
                </div><!-- /.col-* -->

                <div class="col-sm-4">
                    <h2>Stay Connected</h2>

                    <ul class="social-links nav nav-pills">
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                        <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                    </ul><!-- /.header-nav-social -->
                </div><!-- /.col-* -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.footer-top -->

    <div class="footer-bottom">
        <div class="container">
            <div class="footer-bottom-left">
            </div><!-- /.footer-bottom-left -->

            <div class="footer-bottom-right">
                <ul class="nav nav-pills">
                    <li><a href="index-2.html">Home</a></li>
                    <li><a href="pricing.html">Pricing</a></li>
                    <li><a href="terms-conditions.html">Terms &amp; Conditions</a></li>
                    <li><a href="contact-1.html">Contact</a></li>
                </ul><!-- /.nav -->
            </div><!-- /.footer-bottom-right -->
        </div><!-- /.container -->
    </div>
</footer><!-- /.footer -->

</div><!-- /.page-wrapper -->

<script src="assets/js/jquery.js" type="text/javascript"></script>
<script src="assets/js/map.js" type="text/javascript"></script>

<script src="assets/libraries/bootstrap-sass/javascripts/bootstrap/collapse.js" type="text/javascript"></script>
<script src="assets/libraries/bootstrap-sass/javascripts/bootstrap/carousel.js" type="text/javascript"></script>
<script src="assets/libraries/bootstrap-sass/javascripts/bootstrap/transition.js" type="text/javascript"></script>
<script src="assets/libraries/bootstrap-sass/javascripts/bootstrap/dropdown.js" type="text/javascript"></script>
<script src="assets/libraries/bootstrap-sass/javascripts/bootstrap/tooltip.js" type="text/javascript"></script>
<script src="assets/libraries/bootstrap-sass/javascripts/bootstrap/tab.js" type="text/javascript"></script>
<script src="assets/libraries/bootstrap-sass/javascripts/bootstrap/alert.js" type="text/javascript"></script>

<script src="assets/libraries/colorbox/jquery.colorbox-min.js" type="text/javascript"></script>

<script src="assets/libraries/flot/jquery.flot.min.js" type="text/javascript"></script>
<script src="assets/libraries/flot/jquery.flot.spline.js" type="text/javascript"></script>

<script src="assets/libraries/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>

<script src="http://maps.googleapis.com/maps/api/js?libraries=weather,geometry,visualization,places,drawing&amp;sensor=false" type="text/javascript"></script>

<script type="text/javascript" src="assets/libraries/jquery-google-map/infobox.js"></script>
<script type="text/javascript" src="assets/libraries/jquery-google-map/markerclusterer.js"></script>
<script type="text/javascript" src="assets/libraries/jquery-google-map/jquery-google-map.js"></script>

<script type="text/javascript" src="assets/libraries/owl.carousel/owl.carousel.js"></script>
<script type="text/javascript" src="assets/libraries/bootstrap-fileinput/fileinput.min.js"></script>

<script src="assets/js/superlist.js" type="text/javascript"></script>

</body>

<!-- Mirrored from preview.byaviators.com/template/superlist/faq.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 21 Nov 2015 16:10:32 GMT -->
</html>
