@extends('include.header')
@section('header_c')
    <?php

        $data= $profileInfo;
    ?>
    <div class="main">
        <div class="main-inner">

            <div class="content">

                <div class="document-title" style="margin-bottom:0px;">
                    <h1>HEVENTLY Venues & Events Professionals</h1>
                    <div class="text-center custom-white" style="z-index:10000000;">Making your events better with quality and proven event professionals.</div>


                </div><!-- /.document-title -->

                <div class="container">
                    <div class="form">
                       <form action="provider/{!! $data['data']['link'] !!}/hire" method="POST" role="form">
                            <div class="col-lg-12">
                                    <h1><span class="badge" style="border-radius: 100%; width: 40px; height: 40px; display: inline-block; border: groove 1px #ccc;"><h3 style="display: block; margin: 1px 0 0 0;">1</h3></span> What type of work do you require?</h1>
                                    <div class="col-lg-8 col-sm-4" style="backgroud-color: #000;">
                                        <div class="col-md-offset-1">
                                            <span class="text-info">Complete step 1 to unlock step 2</span>
                                            <div class="form-group">
                                                <select class="form-control" name="work_spec">
                                                    <option value="Event">Event</option>
                                                </select>
                                            </div>
                                            <div class="form-group">

                                                <select name="profession_needed" class="form-control" id="step_1_pro_needed">
                                                    <option value="">Needed profession</option>
                                                    <!-- call tdata from the post request -->
                                                      <option value="{!! $data['data']['category'] !!}">{!! $data['data']['category'] !!}</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 step2" id="disable">
                                    <h1><span class="badge" style="border-radius: 100%; width: 40px; height: 40px; display: inline-block; border: groove 1px #ccc;"><h3 style="display: block; margin: 1px 0 0 0;">2</h3></span> What is your event all about</h1>
                                    <div class="col-lg-8 col-sm-4" style="backgroud-color: #000;">
                                        <div class="col-md-offset-1">
                                            <div class="form-group">
                                                <select class="form-control" name="work_about" id="select">
                                                    <option value="">Select event type</option>
                                                    <option value="International Conference">International Conference</option>
                                                    <option value=">General Conference">General Conference</option>
                                                    <option value="Official Meeting">Official Meeting</option>
                                                    <option value="Wedding Ceremony">Wedding Ceremony</option>
                                                    <option value="Burial Ceremony">Burial Ceremony</option>
                                                    <option value="Birthday Ceremony">Birthday Ceremony</option>
                                                    <option value="School Ceremony">School Ceremony</option>
                                                </select>

                                            </div>

                                            <div class="form-group">
                                                <label for="login-form-last-name">Address</label>
                                                <input type="text" class="form-control" value="{!! isset($data['inputs']->address)? $data['inputs']->address: '' !!}" name="address" id="address" disabled >

                                                <input type="hidden" value="{!! isset($data['inputs']->longitude)? $data['inputs']->longitude: '' !!}" name="longitude" id="long">
                                                <input type="hidden" name="latitude"  value="{!! isset($data['inputs']->latitude)? $data['inputs']->latitude: '' !!}"  id="lat">
                                            </div><!-- /.form-group -->
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 step3" id="disable">
                                    <h1><span class="badge" style="border-radius: 100%; width: 40px; height: 40px; display: inline-block; border: groove 1px #ccc;"><h3 style="display: block; margin: 1px 0 0 0;">3</h3></span> Tell us more about your event (including the skills required)</h1>
                                    <div class="col-lg-8 col-sm-4" style="backgroud-color: #000;">
                                        <div class="col-md-offset-1">
                                            <div class="form-group">
                                                <textarea cols="" id="step3_select" name="event_info" style="min-height: 100px;" class="form-control" placeholder="Describe what your event is all about, the skill you are expecting interested Hevently service providers " disabled>{!! isset($data['inputs']->event_info)? $data['inputs']->event_info: '' !!}</textarea>
                                            </div>

                                            <div class="col-lg-8 col-lg-4">
                                                <div class="col-lg-offset-8">
                                                    <div class="form-group">
                                                        <button value="continue" style="width: 100%;" disabled />
                                                        sdknsd iunj
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-12 step4" id="disable">
                                    <h1><span class="badge" style="border-radius: 100%; width: 40px; height: 40px; display: inline-block; border: groove 1px #ccc;"><h3 style="display: block; margin: 1px 0 0 0;">4</h3></span> What budget do you have in mind?</h1>
                                    <div class="col-lg-8 col-sm-4" style="backgroud-color: #000;">
                                        <div class="col-md-offset-1">
                                            <div class="form-group">
                                                <label for="min-budg">Minimum Budget(Naira):</label>
                                                <input type="number" name="min_budget" value="{!! isset($data['inputs']->min_budget)? $data['inputs']->min_budjet: '' !!}" class="form-control" min="100" max="1000000000"  placeholder="Budget range" disabled>

                                                <label for="min-budg">Maximun Budget(Naira):</label>
                                                <input type="number" name="max_budget" value="{!! isset($data['inputs']->max_budget)? $data['inputs']->max_budjet: '' !!}" class="form-control" min="100" max="1000000000"  placeholder="Budget range" disabled>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-12 step5" id="disable">
                                    <h1><span class="badge" style="border-radius: 100%; width: 40px; height: 40px; display: inline-block; border: groove 1px #ccc;"><h3 style="display: block; margin: 1px 0 0 0;">5</h3></span> Deadline for posted project</h1>
                                    <div class="col-lg-8 col-sm-4" style="backgroud-color: #000;">
                                        <div class="col-md-offset-1">
                                            <div class="form-group">
                                                <input type="text" name="deadline" value="{!! isset($data['inputs']->deadline)? $data['inputs']->deadline: '' !!}" class="form-control" placeholder="Expiry date for work. Format: dd/mm/yyyy Hour:Minute e.g 24/03/1016 23:32" disabled>
                                            </div>
                                        </div>
                                    </div>
                                </div>



                                <div class="col-lg-12 step6" id="disable">
                                    <h1><span class="badge" style="border-radius: 100%; width: 40px; height: 40px; display: inline-block; border: groove 1px #ccc;"><h3 style="display: block; margin: 1px 0 0 0;">6</h3></span> Submit</h1>
                                    <div class="col-lg-8 col-sm-4" style="backgroud-color: #000;">
                                        <div class="col-md-offset-1">
                                            <div class="form-group">
                                                <span class="text-info" style="font-size: 12px; font-stretch: extra-expanded;">By clicking on the post button, you have agreed to the terms and conditions of hevently</span>
                                                <button type="submit" class="form-control" disabled>Post to Hevently</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {!! csrf_field() !!}
                            </form>
                            <script type="text/javascript">
                                //Autocomplete variables
                                var input = document.getElementById('address');
                                var lon2 = document.getElementById('long');
                                var lat2 = document.getElementById('lat');
                                var searchform = document.getElementById('form1');
                                var place;
                                var autocomplete = new google.maps.places.Autocomplete(input);

                                //Google Map variables
                                var map;
                                var marker;

                                //Add listener to detect autocomplete selection
                                google.maps.event.addListener(autocomplete, 'place_changed', function () {
                                    place = autocomplete.getPlace();
                                    console.log(place.geometry.location.lat());
                                    lat2.value = place.geometry.location.lat();
                                    lon2.value = place.geometry.location.lng();
                                });
                                //Add listener to search
                                searchform.addEventListener("submit", function() {
                                    var newlatlong = new google.maps.LatLng(place.geometry.location.lat(),place.geometry.location.lng());
                                    map.setCenter(newlatlong);
                                    marker.setPosition(newlatlong);
                                    map.setZoom(12);

                                });

                                //Reset the inpout box on click
                                input.addEventListener('click', function(){
                                    input.value = "";

                                });



                                function initialize() {
                                    var myLatlng = new google.maps.LatLng(51.517503,-0.133896);
                                    var mapOptions = {
                                        zoom: 1,
                                        center: myLatlng
                                    }
                                    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

                                    marker = new google.maps.Marker({
                                        position: myLatlng,
                                        map: map,
                                        title: 'Main map'
                                    });
                                }

                                google.maps.event.addDomListener(window, 'load', initialize);

                            </script>

                    </div>
                </div><!-- /.content -->
            </div><!-- /.container -->
        </div><!-- /.main-inner -->
    </div><!-- /.main -->
@endsection