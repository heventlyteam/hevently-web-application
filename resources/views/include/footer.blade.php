@extends('include.script')
@section('all')
@yield('whol')
<footer class="footer" style="margin-top:0px;padding-top:0px;">
    <div class="footer-top" style="margin-top:0px;">
        <div class="container">
            <div class="row">
               <ul class="list-unstyled list-inline text-center">
                   <li><i class="fa fa-facebook fa-fw"></i></li>
                   <li><i class="fa fa-twitter fa-fw"></i></li>
                   <li><i class="fa fa-google-plus fa-fw"></i></li>
               </ul>
               <ul class="list-unstyled list-inline text-center">
                    <li><a href="index">Home</a></li>
                    <li><a href="about">About Us</a></li>
                    <li><a href="terms-conditions">Terms &amp; Conditions</a></li>
                    <li><a href="contact">Contact</a></li>
                </ul><!-- /.nav -->

                <p class="text-center">Get in touch</p>
                <p class="text-center"><i class="fa fa-phone fa-fw"></i> +2340908674335, +234568902</p>
                <p class="text-center"><i class="fa fa-message fa-fw"></i> support@Hevently.com</p>
                <p class="text-center">&copy; 2015 Hevently</p>
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.footer-top -->
</footer><!-- /.footer -->
</div>
<div class="modal" id="myModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="padding:35px 50px;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4><span class="glyphicon glyphicon-lock"></span> </h4>
            </div>
            <div class="modal-body" style="padding:40px 50px;">
                <p></p>
            </div>
            <div class="modal-footer">
                <p>HEVENTLY</p>
            </div>
        </div>

    </div>
</div>

    <div class="show_error" style="position: fixed; bottom: 0; width: 100%;"></div>
@endsection