@extends('include.footer')
@section('whol')
<!DOCTYPE html>
<html>
<head>
    <base url="http://localhost:8000/" href="http://localhost:8000/" />
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    
    <link href="assets/libraries/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="assets/libraries/owl.carousel/assets/owl.carousel.css" rel="stylesheet" type="text/css" >
    <link href="assets/libraries/colorbox/example1/colorbox.css" rel="stylesheet" type="text/css" >
    <link href="assets/libraries/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" type="text/css">
    <link href="assets/libraries/bootstrap-fileinput/fileinput.min.css" rel="stylesheet" type="text/css">
    <link href="assets/css/superlist.css" rel="stylesheet" type="text/css" >
    <link rel="stylesheet" type="text/css" href="assets/css/cavve.css">

    <link rel="shortcut icon" type="image/x-icon" href="assets/favicon.png">
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCDNQdnTuLjzxYMO2Tn2NCAlFM53s0lF_U&libraries=places">
</script>
  <title>Hevently - Home</title>
    <style>
        .table > thead > tr > .jobTd,
        .table > thead > tr > .jobTd,
        .table > tbody > tr > .jobTd,
        .table > tbody > tr > .jobTd,
        .table > tfoot > tr > .jobTd,
        .table > tfoot > tr > .jobTd,
        .table > .jobBody > tr > td{
            line-height: 1.4;
            font-weight: 400;
            height: 56px;
            border-top: 0;
            padding-left: 20px;
            vertical-align: middle;
            text-align: left;
        }
        table > thead, table > tbody{

            font-size: .8125rem;
            line-height: 1.4;
            font-weight: 400;
            font-size: 16px;
        }
        table > thead{
            color: #fefeff;

        }
    </style>

    <style>

        .textareacontainer, .iframecontainer {
            height:49%;
            float:none;
            width:100%;
        }
        .textarea, .iframe {
            padding:20px;
        }
        .iframe {
            padding-top:0;
        }
        .container {
            min-width:260px;
        }
        @media screen and (max-height: 900px) {
            .footerText {
                display:none;
            }
        }

    </style>

</head>


<body>

<div class="page-wrapper">
    @yield('top_content')
  
 @endsection