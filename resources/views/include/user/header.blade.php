<div class="row" style="background:#000;color:#fefefe;padding-top:8px;">
   <div class="container">
    <p class="pull-right" style="font-size:12px;padding-right:10px;">Are you an Event Service provider? <a href="serviceprovider/register">Register</a> or <a href="serviceprovider/login">Login</a></p>

    <ul class="list-unstyled list-inline hidden-sm hidden-xs" >
        <li style="font-size:12px;"><i class="fa fa-phone fa-fw"></i> Call us on: +2348749743947</li>
    </ul>
   </div>
 </div>
 <header class="header">
    <div class="header-wrapper">
        <div class="container">
            <div class="header-inner">
                <div class="header-logo">
                    <a href="index">
                        <img src="assets/img/logo.png" alt="Logo">
                        <span></span>
                    </a>
                </div><!-- /.header-logo -->

                <div class="header-content">
                    <div class="header-bottom">
                        
                        <ul class="header-nav-primary nav nav-pills collapse navbar-collapse">
    <li class="active">
        <a href="index">Home</a>
    </li>

    <li >
        <a href="listings">Listings</a>
    </li>

    <li>
        <a href="login">Login</a>
    </li>
    <li>
        <a href="register">Register</a>
    </li>
    <li>
        <a href="about">About</a>
    </li>

    <li >
        <a href="faq">Faq</a>
    </li>
    <li>
        <a href="contact">Contact</a>
    </li>
</ul>

<button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target=".header-nav-primary">
    <span class="sr-only">Toggle navigation</span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
</button>

                    </div><!-- /.header-bottom -->
                </div><!-- /.header-content -->
            </div><!-- /.header-inner -->
        </div><!-- /.container -->
    </div><!-- /.header-wrapper -->
</header><!-- /.header -->