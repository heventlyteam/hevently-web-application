<div class="sidebar-admin">
    <ul>
        <li class="active" ><a href="#dashboard" onclick="loadDashboard()"><i class="fa fa-dashboard"></i></a></li>
        <li class="active" ><a href="#profile" onclick="loadProfile()"><i class="fa fa-inbox"></i></a></li>
        <li class="active" ><a href="settings" onclick="loadSetting()"><i class="fa fa-cog"></i></a></li>
    </ul>
</div><!-- /.sidebar-admin-->
