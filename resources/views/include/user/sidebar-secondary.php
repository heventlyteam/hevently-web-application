 <div class="sidebar-secondary-admin">
                    <ul>
                        <li class="active">
                            <a href="profile.php">
                                <span class="icon"><i class="fa fa-info"></i></span>
                                <span class="title">About</span>
                                <span class="subtitle">Details about your company</span>
                            </a>
                        </li>

                        <li >
                            <a href="gallery.php">
                                <span class="icon"><i class="fa fa-file-image-o"></i></span>
                                <span class="title">Galllery</span>
                                <span class="subtitle">Beautiful Images about you.</span>
                            </a>
                        </li>

                        <li >
                            <a href="admin-tables.html">
                                <span class="icon"><i class="fa fa-briefcase"></i></span>
                                <span class="title">Previous Work</span>
                                <span class="subtitle">Works done prior </span>
                            </a>
                        </li>
                         <li >
                            <a href="admin-grid.html">
                                <span class="icon"><i class="fa fa-facebook-official"></i></span>
                                <span class="title">Social Media</span>
                                <span class="subtitle">Social Media pages</span>
                            </a>
                        </li>
                        <li >
                            <a href="admin-grid.html">
                                <span class="icon"><i class="fa fa-cc-visa"></i></span>
                                <span class="title">Account Settings</span>
                                <span class="subtitle">Payment for services</span>
                            </a>
                        </li>
                         <li>
                            <a href="admin-grid.html">
                                <span class="icon"><i class="fa fa-thumbs-o-up"></i></span>
                                <span class="title">Verification</span>
                                <span class="subtitle">Process verification</span>
                            </a>
                        </li>

                        
                    </ul>
                </div><!-- /.sidebar-secondary-admin -->
