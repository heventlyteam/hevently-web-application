<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">

    <link href="http://fonts.googleapis.com/css?family=Nunito:300,400,700" rel="stylesheet" type="text/css">
    <link href="../assets/libraries/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="../assets/libraries/owl.carousel/assets/owl.carousel.css" rel="stylesheet" type="text/css" >
    <link href="../assets/libraries/colorbox/example1/colorbox.css" rel="stylesheet" type="text/css" >
    <link href="../assets/libraries/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" type="text/css">
    <link href="../assets/libraries/bootstrap-fileinput/fileinput.min.css" rel="stylesheet" type="text/css">
    <link href="../assets/css/superlist.css" rel="stylesheet" type="text/css" >

    <script src="../assets/js/jquery.js" type="text/javascript"></script>
    <script type="text/javascript" src="assets/js/edit_profile.js"></script>
    <link rel="shortcut icon" type="image/x-icon" href="../assets/favicon.png">
    <title>Hevently - Event Professional Dashboard</title>
    <script type="text/javascript">
        $('submit').click(function{
           alert('sss');
        });
    </script>
    <style>
        .alert{
            position: fixed;
            bottom: 0;
            width: 100%;
        }
    </style>
</head>


<body onload="showLoading(false)">

<div class="page-wrapper">
    