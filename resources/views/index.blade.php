@extends('include.header')
@section('header_c')
    <?php
    //check if the user/provider session exists
    $check = new \App\Http\Controllers\LoginSessionManagement();
    $checkUser = $check->validateLoginSession('userinfo');
    $checkProvider = $check->validateLoginSession('sp_info');
    $sessionInfo = new \Illuminate\Support\Facades\Session();
    $userInfo = '';
    if($checkUser)
        $userInfo = $sessionInfo::get('userinfo');
    if($checkProvider)
        $userInfo = $sessionInfo::get('sp_info');

    $getProfessionanls = new \App\Http\Controllers\ProfessionalsController();
    $professional = $getProfessionanls->getProfessionals();
    $pro = json_decode($professional, true);
    ?>
    <div class="main">
                <div class="main-inner">
                    <div class="content">
                        <div class="mt-80">
        <div class="hero-image">
            <div class="hero-image-inner" style="background-image: url('assets/img/tmp/slide2.jpg');">
                <div class="hero-image-content">
                    <div class="container">
                        <h1 style="margin-bottom:20px;">Find.Compare.Connect</h1>
                        <p style="margin-bottom:20px;">Making event organizing easy. Find all the professionals <br>you need to organize an event.</p>
                        <a href="jobs" style="margin-top:10px;" class="btn btn-primary btn-lg">Learn More &raquo;</a>

                        @if(!$checkProvider)
                        <p style="color:#fefefe;margin-top:3%;">Are you an Events Professional <br> <a href="provider/register" class="" style="margin-top:0px;padding-top:0px;">Click here to register &raquo;</a></p>
                        @else

                            <a href="account" style="margin-top:10px;" class="btn btn-primary btn-lg">My Dashboard &raquo;</a>
                        @endif
                    </div><!-- /.container -->
                </div><!-- /.hero-image-content -->

                <div class="hero-image-form-wrapper">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-4 col-sm-offset-8 col-lg-4 col-lg-offset-7">
                                <form method="get" action="/listings">
                                    <h2><i class="fa fa-search fa-fw"></i>Find Professionals</h2>

                                    <div class="hero-image-keyword form-group">
                                        <input type="text" name="keyword" class="form-control" placeholder="Keyword">
                                    </div><!-- /.form-group -->

                                    <div class="hero-image-location form-group">
                                        <input type="text" class="form-control" value="{!! isset($data['inputs']->address)? $data['inputs']->address: '' !!}" name="address" id="address" >

                                        <input type="hidden" value="{!! isset($data['inputs']->longitude)? $data['inputs']->longitude: '' !!}" name="longitude" id="long">
                                        <input type="hidden" name="latitude"  value="{!! isset($data['inputs']->latitude)? $data['inputs']->latitude: '' !!}"  id="lat">

                                    </div><!-- /.form-group -->

                                    <div class="hero-image-category form-group">
                                        <select  name="category" class="form-control" title="Category">
                                            <option value="all" selected="">All</option>
                                            @foreach($pro['data'] as $pr)
                                                <option value="{!! $pr['name'] !!}">{!! $pr['name'] !!}</option>
                                            @endforeach
                                        </select>
                                    </div><!-- /.form-group -->

                                    <div class="hero-image-price form-group">
                                        <input type="text" class="form-control" placeholder="Budget Price (can be left blank)">
                                    </div><!-- /.form-group -->

                                    <button type="submit" class="btn btn-default btn-block">Search</button>
                                </form>
                                <script type="text/javascript">
                                    //Autocomplete variables
                                    var input = document.getElementById('address');
                                    var lon2 = document.getElementById('long');
                                    var lat2 = document.getElementById('lat');
                                    var searchform = document.getElementById('form1');
                                    var place;
                                    var autocomplete = new google.maps.places.Autocomplete(input);

                                    //Google Map variables
                                    var map;
                                    var marker;

                                    //Add listener to detect autocomplete selection
                                    google.maps.event.addListener(autocomplete, 'place_changed', function () {
                                        place = autocomplete.getPlace();
                                        console.log(place.geometry.location.lat());
                                        lat2.value = place.geometry.location.lat();
                                        lon2.value = place.geometry.location.lng();
                                    });
                                    //Add listener to search
                                    searchform.addEventListener("submit", function() {
                                        var newlatlong = new google.maps.LatLng(place.geometry.location.lat(),place.geometry.location.lng());
                                        map.setCenter(newlatlong);
                                        marker.setPosition(newlatlong);
                                        map.setZoom(12);

                                    });

                                    //Reset the inpout box on click
                                    input.addEventListener('click', function(){
                                        input.value = "";

                                    });



                                    function initialize() {
                                        var myLatlng = new google.maps.LatLng(51.517503,-0.133896);
                                        var mapOptions = {
                                            zoom: 1,
                                            center: myLatlng
                                        }
                                        map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

                                        marker = new google.maps.Marker({
                                            position: myLatlng,
                                            map: map,
                                            title: 'Main map'
                                        });
                                    }

                                    google.maps.event.addDomListener(window, 'load', initialize);

                                </script>
                            </div><!-- /.col-* -->
                        </div><!-- /.row -->
                    </div><!-- /.container -->
                </div><!-- /.hero-image-form-wrapper -->
            </div><!-- /.hero-image-inner -->
        </div><!-- /.hero-image -->

        </div>
        <div class="container">

            <?php
            //temporarily disable mpst recent places
            $show = false;
            if($show){
            ?>
            <div class="page-header" style="margin-top:40px;">
                <h1>Most Recent Places</h1>
                <p>List of most recent event places to organize your events</p>
            </div><!-- /.page-header -->
        <?php
            //tihis is to get the oist of the sp on the server
            $loadSp = new \App\Http\Controllers\LoadAllSP();
                $response = $loadSp->loadAll();
                //this gets an array fro the controller
        ?>
            @if(count($reponse['data']) > 0)
                @foreach($response['data'] as $sp)
            <div class="cards-simple-wrapper">
                <div class="row">
                    <div class="col-md-4 col-sm-6">
                        <div class="custom-card">
                            <h2 class="" style="position:absolute;color:#fefefe;margin-top:0px;padding:5px;background:#f00;"><i class="fa fa-heart-o"></i></h2>
                            <img src="{!! $sp['background'] !!}" class="img-responsive" alt="Decorators" />
                            <div class="custom-card-in">
                                <h3><a href="listing-detail/{!! $sp['link'] !!}">{!! $sp['bizname'] !!}</a></h3>
                                <div class="pull-right custom-star-color" style="margin-top:0px;padding-top:0px;">
                                    <h3 style="margin-top:0px;padding-top:0px;">N25000 <br><small>per day</small></h3>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div><!-- /.card-rating -->
                                <ul class="list-unstyled">
                                    <li><i class="fa fa-user fa-fw"></i> 2000 people</li>

                                </ul>

                                <p class="text-muted"><i class="fa fa-map-marker fa-fw"></i> Victoria Island, Lagos</p>
                                <div class="clearfix"></div>
                            </div><!-- /card-in -->
                        </div>
                    </div>
                    @endforeach
                    @else
                    <h2>No Recent place has been added.</h2>
                     @endif
                </div><!-- /.row -->
            </div><!-- /.cards-simple-wrapper -->



                    <?php
                    //end of diable
                    }
                    ?>
                    <?php
                        $show = false;
                            if($show){
                     ?>
        <div class="block background-white fullwidth mt80">
        <div class="row">
        <h1 class="text-center">Popular Service Providers</h1>

            <div class="col-sm-6 col-md-4">
                <div class="card-simple" data-background-image="assets/img/tmp/product-2.jpg">
                    <div class="card-simple-background">
                        <div class="card-simple-content">
                            <h2><a href="listing-detail">Queens Bee Decorators</a></h2>
                            <h6 class="text-center custom-white"><i class="fa fa-map-marker"></i> Victoria Island Lagos</h6>
                            <div class="card-simple-rating">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                            </div><!-- /.card-rating -->

                            <div class="card-simple-actions">
                                <a href="#" class="fa fa-bookmark-o"></a>
                                <a href="listing-detail" class="fa fa-search"></a>
                                <a href="#" class="fa fa-heart-o"></a>
                            </div><!-- /.card-simple-actions -->
                        </div><!-- /.card-simple-content -->

                        <div class="card-simple-price">Photographer</div>

                    </div><!-- /.card-simple-background -->
                </div><!-- /.card-simple -->
            </div><!-- /.col-* -->
            <div class="col-sm-6 col-md-4">
                <div class="card-simple" data-background-image="assets/img/tmp/product-2.jpg">
                    <div class="card-simple-background">
                        <div class="card-simple-content">
                            <h2><a href="listing-detail">Queens Bee Decorators</a></h2>
                            <h6 class="text-center custom-white"><i class="fa fa-map-marker"></i> Victoria Island Lagos</h6>
                            <div class="card-simple-rating">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                            </div><!-- /.card-rating -->

                            <div class="card-simple-actions">
                                <a href="#" class="fa fa-bookmark-o"></a>
                                <a href="listing-detail" class="fa fa-search"></a>
                                <a href="#" class="fa fa-heart-o"></a>
                            </div><!-- /.card-simple-actions -->
                        </div><!-- /.card-simple-content -->

                        <div class="card-simple-price">Decorator</div>

                    </div><!-- /.card-simple-background -->
                </div><!-- /.card-simple -->
            </div><!-- /.col-* -->

            <div class="col-sm-6 col-md-4">
                <div class="card-simple" data-background-image="assets/img/tmp/product-2.jpg">
                    <div class="card-simple-background">
                        <div class="card-simple-content">
                            <h2><a href="listing-detail">Queens Bee Decorators</a></h2>
                            <div class="card-meta">
                                <i class="fa fa-map-marker"></i> Victoria Island Lagos
                            </div><!-- /.card-meta -->
                            <div class="card-simple-rating">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                            </div><!-- /.card-rating -->

                            <div class="card-simple-actions">
                                <a href="#" class="fa fa-bookmark-o"></a>
                                <a href="listing-detail" class="fa fa-search"></a>
                                <a href="#" class="fa fa-heart-o"></a>
                            </div><!-- /.card-simple-actions -->
                        </div><!-- /.card-simple-content -->

                        <div class="card-simple-price">Caterers</div>

                    </div><!-- /.card-simple-background -->
                </div><!-- /.card-simple -->
            </div><!-- /.col-* -->


            <div class="col-sm-6 col-md-4">
                <div class="card-simple" data-background-image="assets/img/tmp/product-2.jpg">
                    <div class="card-simple-background">
                        <div class="card-simple-content">
                            <h2><a href="listing-detail">Queens Bee Decorators</a></h2>
                            <div class="card-meta">
                                <i class="fa fa-map-marker"></i> Victoria Island Lagos
                            </div><!-- /.card-meta -->

                            <div class="card-simple-rating">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                            </div><!-- /.card-rating -->

                            <div class="card-simple-actions">
                                <a href="#" class="fa fa-bookmark-o"></a>
                                <a href="listing-detail" class="fa fa-search"></a>
                                <a href="#" class="fa fa-heart-o"></a>
                            </div><!-- /.card-simple-actions -->
                        </div><!-- /.card-simple-content -->

                        <div class="card-simple-price">Caterers</div>

                    </div><!-- /.card-simple-background -->
                </div><!-- /.card-simple -->
            </div><!-- /.col-* -->


            <div class="col-sm-6 col-md-4">
                <div class="card-simple" data-background-image="assets/img/tmp/product-2.jpg">
                    <div class="card-simple-background">
                        <div class="card-simple-content">
                            <h2><a href="listing-detail">Queens Bee Decorators</a></h2>
                            <div class="card-meta">
                                <i class="fa fa-map-marker"></i> Victoria Island Lagos
                            </div><!-- /.card-meta -->
                            <div class="card-simple-rating">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                            </div><!-- /.card-rating -->

                            <div class="card-simple-actions">
                                <a href="#" class="fa fa-bookmark-o"></a>
                                <a href="listing-detail" class="fa fa-search"></a>
                                <a href="#" class="fa fa-heart-o"></a>
                            </div><!-- /.card-simple-actions -->
                        </div><!-- /.card-simple-content -->

                        <div class="card-simple-price red-bg">Caterers</div>

                    </div><!-- /.card-simple-background -->
                </div><!-- /.card-simple -->
            </div><!-- /.col-* -->


            <div class="col-sm-6 col-md-4">
                <div class="card-simple" data-background-image="assets/img/tmp/product-2.jpg">
                    <div class="card-simple-background">
                        <div class="card-simple-content">
                            <h2><a href="listing-detail">Queens Bee Decorators</a></h2>
                            <div class="card-meta">
                                <i class="fa fa-map-marker"></i> Victoria Island Lagos
                            </div><!-- /.card-meta -->
                            <div class="card-simple-rating">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                            </div><!-- /.card-rating -->

                            <div class="card-simple-actions">
                                <a href="#" class="fa fa-bookmark-o"></a>
                                <a href="listing-detail" class="fa fa-search"></a>
                                <a href="#" class="fa fa-heart-o"></a>
                            </div><!-- /.card-simple-actions -->
                        </div><!-- /.card-simple-content -->

                        <div class="card-simple-price">Caterers</div>

                    </div><!-- /.card-simple-background -->
                </div><!-- /.card-simple -->
            </div><!-- /.col-* -->


                </div><!-- /.row -->

            </div><!-- /.block -->
            <?php
                 }

            ?>
            <div class="block background-black-light fullwidth">
                <div class="row">
                <h1 class="text-center custom-white">Hevently Makes It Simple for you</h1>
                <p class="text-center">Contact event proffesionals at ease today.</p>
            <div class="col-sm-4">
                <div class="box">
                    <a href="listings" style="text-decoration: none; color: inherit;">
                        <div class="box-icon">
                            <i class="fa fa-search"></i>
                        </div><!-- /.box-icon -->
                    </a>
                    <div class="box-content">
                        <h2>Search for a proffesional</h2>
                        <p>Search from our list of verified event professionals and get best quality professionals in your event area.</p>

                    </div><!-- /.box-content -->
                </div>
            </div><!-- /.col-sm-4 -->

            <div class="col-sm-4">
                <div class="box">
                    <a href="listings" style="text-decoration: none; color: inherit;">
                      <div class="box-icon">
                          <i class="fa fa-mouse-pointer"></i>
                      </div><!-- /.box-icon -->
                    </a>
                    <div class="box-content">
                        <h2>Choose Event Professional</h2>
                        <p>Select from the list of verified events professsional the ones you want.</p>

                    </div><!-- /.box-content -->
                </div>
            </div><!-- /.col-sm-4 -->

            <div class="col-sm-4">
                <div class="box">
                    <div class="box-icon">
                        <i class="fa fa-thumbs-o-up"></i>
                    </div><!-- /.box-icon -->

                    <div class="box-content">
                        <h2>Contact Event Professional</h2>
                        <p>The event profesional reviews your budget for the event and decides to bid for your event.</p>
                    </div><!-- /.box-content -->
                </div>
            </div><!-- /.col-sm-4 -->
        </div><!-- /.row -->

            </div><!-- /.block -->

            <div class="block background-white background-transparent-image fullwidth">
                <div class="page-header">
            <h1>Search for the best Events Professionals</h1>
            <p>Check out the best events&amp; professional in your city. Verified event professionals<br> with the power of search we become who we are.</p>
        </div><!-- /.page-header -->

        <div class="cards-wrapper">
            @if(count($pro['data']) == 0)

                <div class="row">
                    <!-- col3 -->
                    <div class="col-lg-12">
                <span class="text text-warning">No professional has been added for now check back later</span>
                        </div>
                    </div>
            @else
            <div class="row">
                <!-- col3 -->
               <div class="col-md-12">
                @foreach($pro['data'] as $pr)
                  <div class="col-md-6">
                   <!-- card-abs -->
                    <div style="background:url('{!! $pr['image'] !!}') no-repeat;background-size:100%;padding:50px;margin:10px;">
                      <a href="#">
                      <h3 class="text-center custom-white"><b>{!! $pr['name'] !!}</b></h3>
                      <p class="text-center custom-white">{!! $pr['description'] !!}</p>
                      </a>
                    </div>
                    <!-- /card-abs -->
                  </div>
                <!--  /col3 -->
               @endforeach
                   </div>
            </div><!-- /.row -->
                @endif
        </div><!-- /.cards-wrapper -->

            </div>

            <div class="block background-white fullwidth" style="margin-bottom:0px;padding-bottom:0px;">
                <div class="page-header">
            <h1>People Reviews</h1>
            <p>Read what people are saying about Hevently.</p>
        </div><!-- /.page-header -->
        <!--load all preview -->

        <div class="row">
          <div class="container">
            <div class="col-sm-6">
                <div class="testimonial">
                    <div class="testimonial-image">
                        <img src="assets/img/tmp/agent-1.jpg" alt="">
                    </div><!-- /.testimonial-image -->

                    <div class="testimonial-inner">
                        <div class="testimonial-title">
                            <h2>Exactly What I Needed</h2>

                            <div class="testimonial-rating">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                            </div><!-- /.testimonial-rating -->
                        </div><!-- /.testimonial-title -->

                        Thanks to Hevently because I got exactly what I needed a which was I Decorator
                        <div class="testimonial-sign">- Nwokolo Richard</div><!-- /.testimonial-sign -->
                    </div><!-- /.testimonial-inner -->
                </div><!-- /.testimonial -->
            </div><!-- /.col-* -->

            <div class="col-sm-6">
                <div class="testimonial last">
                    <div class="testimonial-image">
                        <img src="assets/img/tmp/agent-5.jpg" alt="">
                    </div><!-- /.testimonial-image -->

                    <div class="testimonial-inner">
                        <div class="testimonial-title">
                            <h2>Hevently made my son's 10th year birthday </h2>

                            <div class="testimonial-rating">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                            </div><!-- /.testimonial-rating -->
                        </div><!-- /.testimonial-title -->

                        I am proud to have an account on Hevently. (Thumbs up)
                        <div class="testimonial-sign">- Micheal Fade</div><!-- /.testimonial-sign -->
                    </div><!-- /.testimonial-inner -->
                </div><!-- /.testimonial -->
            </div><!-- /.col-* -->
        </div>
        </div><!-- /container -->
        </div><!-- /.container -->

        <div class="row" style="margin-top:7%;">
          <div class="container">
            <div class="col-md-5">
              <div class="custom-center">
                <img src="assets/img/tmp/sales.png" class="img-reponsive pull-right" width="60%">
              </div>
            </div>
            <div class="col-md-6 col-md-offset-1">
                <h2 style="font-size:36px">Do you offer services in the <br>event industry such as catering, photography etc?</h2>
                <h5 style="font-size:24px;font-weight:200">Increase sales today, register and let more <br>people discover about your services.</h5>
                <a href="#services.php" style="margin-top:30px" class="btn btn-primary btn-lg">Learn More &raquo;</a>
            </div>
          </div>
        </div>

                    </div><!-- /.content -->
                </div><!-- /.main-inner -->
            </div><!-- /.main -->
        <!--dont forget script -->
    </div>
        @endsection