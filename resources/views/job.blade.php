@extends('include.header')
@section('header_c')
    <?php

    $getProfessionanls = new \App\Http\Controllers\ProfessionalsController();
    $professional = $getProfessionanls->getProfessionals();
    $pro = json_decode($professional, true);
    ?>
    <div class="main">
        <div class="main-inner">
            
                <div class="content">
                    
    <div class="document-title" style="margin-bottom:0px;">
        <h1>HEVENTLY Venues & Events Professionals</h1>
        <div class="text-center custom-white" style="z-index:10000000;">Making your events better with quality and proven event professionals.</div>

       
    </div><!-- /.document-title -->



                    <form class="filter" method="get" action="">
    <div class="row">
        <div class="col-sm-12  col-md-offset-2 col-md-3">
            <div class="form-group">
                <input type="text" class="form-control" value="{!! isset($data['inputs']->address)? $data['inputs']->address: '' !!}" name="address" id="address" >

                <input type="hidden" value="{!! isset($data['inputs']->longitude)? $data['inputs']->longitude: '' !!}" name="longitude" id="long">
                <input type="hidden" name="latitude"  value="{!! isset($data['inputs']->latitude)? $data['inputs']->latitude: '' !!}"  id="lat">

            </div><!-- /.form-group -->
        </div><!-- /.col-* -->

        <div class="col-sm-12 col-md-3">
            <div class="form-group">
                <select class="form-control" name="category">
                    <option value="all" selected="">All</option>
                    @foreach($pro['data'] as $pr)
                        <option value="{!! $pr['name'] !!}">{!! $pr['name'] !!}</option>
                    @endforeach
                </select>
            </div><!-- /.form-group -->
        </div><!-- /.col-* -->


        <div class="col-sm-12 col-md-2">
            <button type="submit" class="btn btn-primary pull-left"><i class="fa fa-search fa-fw"></i> Search</button>
        </div><!-- /.col-* -->
        <div class="col-sm-12 col-md-2">
            <a href="jobs/post" class="btn btn-primary pull-left"><i class="fa fa-plus fa-fw"></i> Post Job</a>
        </div><!-- /.col-* -->


    </div><!-- /.row -->
</form>

                    <script type="text/javascript">
                        //Autocomplete variables
                        var input = document.getElementById('address');
                        var lon2 = document.getElementById('long');
                        var lat2 = document.getElementById('lat');
                        var searchform = document.getElementById('form1');
                        var place;
                        var autocomplete = new google.maps.places.Autocomplete(input);

                        //Google Map variables
                        var map;
                        var marker;

                        //Add listener to detect autocomplete selection
                        google.maps.event.addListener(autocomplete, 'place_changed', function () {
                            place = autocomplete.getPlace();
                            console.log(place.geometry.location.lat());
                            lat2.value = place.geometry.location.lat();
                            lon2.value = place.geometry.location.lng();
                        });
                        //Add listener to search
                        searchform.addEventListener("submit", function() {
                            var newlatlong = new google.maps.LatLng(place.geometry.location.lat(),place.geometry.location.lng());
                            map.setCenter(newlatlong);
                            marker.setPosition(newlatlong);
                            map.setZoom(12);

                        });

                        //Reset the inpout box on click
                        input.addEventListener('click', function(){
                            input.value = "";

                        });



                        function initialize() {
                            var myLatlng = new google.maps.LatLng(51.517503,-0.133896);
                            var mapOptions = {
                                zoom: 1,
                                center: myLatlng
                            }
                            map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

                            marker = new google.maps.Marker({
                                position: myLatlng,
                                map: map,
                                title: 'Main map'
                            });
                        }

                        google.maps.event.addDomListener(window, 'load', initialize);

                    </script>
<div class="container">
<h2 class="page-title">

   <form method="get" action="http://preview.byaviators.com/template/superlist/listing-grid.html?" class="filter-sort">
        <div class="form-group">
            <select title="Sort by">
                <option name="price">Location</option>
                <option name="rating">Rating</option>
                <option name="title">Ratings</option>
            </select>
        </div><!-- /.form-group -->

    </form>
</h2><!-- /.page-title -->
                <div class="table-responsive cover-table">
                    <table class="table table-reponsive tdColor" style="">
                        <thead class="active" style="">
                            <tr style="height: 50px; background-color: #c0c0c0;">
                                <td class="jobTd">PROJECT/CONTEST</td>
                                <td class="jobTd">CATEGORY</td>
                                <td class="jobTd">STARTED</td>
                                <td class="jobTd">BUDGET</td>
                            </tr>
                        </thead>
                        <tbody class="jobBody" style="min-height: 200px;">
                        <tr>
                            @if(isset($data['all_jobs']['data']) && count($data['all_jobs']['data']) > 0)
                                @foreach($data['all_jobs']['data'] as $dt)
                            <td style="-webkit-border-radius: 0px;-moz-border-radius: 0px;border-radius: 0px;">
                                <h2 class="active" style="color: #5555C4;">
                                    <i class="fa fa-desktop"></i>
                                    <a href="#wik" class="link2" onclick='loadJob("{!! $dt['link'] !!}")' style="color: #5555C4;">{!! is_array($dt['job']['event_type']) ? '' : $dt['job']['event_type'] !!}</a> by {!! $dt['username'] !!}
                                </h2>
                                <p class="jobHead">
                                    {!! $dt['job']['description'] !!}
                                </p>
                            </td>
                            <td style="text-align: center;"> {!! $dt['job']['title'] !!}</td>
                            <td>
                                <?php
                                    $time =  $dt['created_at']['sec'];
                                    //convert useing timestamp
                                     $d = date('d',$time);
                                     $y = date('y',$time);
                                     $m = date('m',$time);
                                    echo $d.'/'.$m.'/'.$y;
                                ?>
                            </td>
                            <td><?php
                                    //
                                    $bud = $dt['job']['budget'];
                                    $exBud = explode(' ', $bud);
                                    $minBud= $exBud[0];
                                    $maxBud = (isset($exBud[1])) ? $exBud[1] : '';
                                    echo '#'.$minBud.' - #'.$maxBud;
                                ?>
                            </td>
                        </tr>
                        @endforeach
                            @else
                                <tr style="-webkit-border-radius: 0px;-moz-border-radius: 0px;border-radius: 0px;">
                                    <h2 class="active" style="color: #5555C4;">
                                        <i class="fa fa-desktop"></i>
                                        <a href="#"  style="color: #5555C4;">No jobs available at the moment</a>
                                    </h2>
                                </tr>
                            @endif
                        </tbody>
                    </table>

                 <script>
                     $('a').click(function(e){
                         alert();
                     });
                 </script>
             </div>

<div class="pager">
    <ul>
        <li><a href="#">Prev</a></li>
        <li><a href="#">5</a></li>
        <li class="active"><a>6</a></li>
        <li><a href="#">7</a></li>
        <li><a href="#">Next</a></li>
    </ul>
</div><!-- /.pagination -->

<h2 class="page-title">Related Searches</h2>
<div class="cards-simple-wrapper">
    <div class="row">

            <div class="col-sm-6 col-md-4 col-lg-3">
                <div class="card-simple" data-background-image="assets/img/tmp/product-1.jpg">
                    <div class="card-simple-background">
                        <div class="card-simple-content">
                            <h2><a href="listing-detail">Trip To Paris, France</a></h2>
                            <div class="card-simple-rating">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                            </div><!-- /.card-rating -->

                            <div class="card-simple-actions">
                                <a href="#" class="fa fa-star-o"></a>
                                <a href="listing-detail" class="fa fa-search"></a>
                                <a href="#" class="fa fa-heart-o"></a>
                            </div><!-- /.card-simple-actions -->
                        </div><!-- /.card-simple-content -->

                        <div class="card-simple-label">Vacation</div>

                            <div class="card-simple-price">$100 / night</div>

                    </div><!-- /.card-simple-background -->
                </div><!-- /.card-simple -->
            </div><!-- /.col-* -->

            <div class="col-sm-6 col-md-4 col-lg-3">
                <div class="card-simple" data-background-image="assets/img/tmp/product-1.jpg">
                    <div class="card-simple-background">
                        <div class="card-simple-content">
                            <h2><a href="listing-detail">Trip To Paris, France</a></h2>
                            <div class="card-simple-rating">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                            </div><!-- /.card-rating -->

                            <div class="card-simple-actions">
                                <a href="#" class="fa fa-star-o"></a>
                                <a href="listing-detail" class="fa fa-search"></a>
                                <a href="#" class="fa fa-heart-o"></a>
                            </div><!-- /.card-simple-actions -->
                        </div><!-- /.card-simple-content -->

                        <div class="card-simple-label">Vacation</div>

                            <div class="card-simple-price">$100 / night</div>

                    </div><!-- /.card-simple-background -->
                </div><!-- /.card-simple -->
            </div><!-- /.col-* -->

            <div class="col-sm-6 col-md-4 col-lg-3">
                <div class="card-simple" data-background-image="assets/img/tmp/product-1.jpg">
                    <div class="card-simple-background">
                        <div class="card-simple-content">
                            <h2><a href="listing-detail">Trip To Paris, France</a></h2>
                            <div class="card-simple-rating">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                            </div><!-- /.card-rating -->

                            <div class="card-simple-actions">
                                <a href="#" class="fa fa-star-o"></a>
                                <a href="listing-detail" class="fa fa-search"></a>
                                <a href="#" class="fa fa-heart-o"></a>
                            </div><!-- /.card-simple-actions -->
                        </div><!-- /.card-simple-content -->

                        <div class="card-simple-label">Vacation</div>

                            <div class="card-simple-price">$100 / night</div>

                    </div><!-- /.card-simple-background -->
                </div><!-- /.card-simple -->
            </div><!-- /.col-* -->

            <div class="col-sm-6 col-md-4 col-lg-3">
                <div class="card-simple" data-background-image="assets/img/tmp/product-1.jpg">
                    <div class="card-simple-background">
                        <div class="card-simple-content">
                            <h2><a href="listing-detail">Trip To Paris, France</a></h2>
                            <div class="card-simple-rating">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                            </div><!-- /.card-rating -->

                            <div class="card-simple-actions">
                                <a href="#" class="fa fa-star-o"></a>
                                <a href="listing-detail" class="fa fa-search"></a>
                                <a href="#" class="fa fa-heart-o"></a>
                            </div><!-- /.card-simple-actions -->
                        </div><!-- /.card-simple-content -->

                        <div class="card-simple-label">Vacation</div>

                            <div class="card-simple-price">$100 / night</div>

                    </div><!-- /.card-simple-background -->
                </div><!-- /.card-simple -->
            </div><!-- /.col-* -->
        </div><!-- /row -->
    </div><!-- /cards-simple-wrapper -->




                </div><!-- /.content -->
            </div><!-- /.container -->
        </div><!-- /.main-inner -->
    </div><!-- /.main -->

  @endsection