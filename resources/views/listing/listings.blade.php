@extends('include.header')
@section('header_c')
    <?php
        $getProf = new \App\Http\Controllers\ProfessionalsController();
        $professionals2 = $getProf->getProfessionals();
        $professionals = json_decode($professionals2, true);
    ?>
    <div class="main">
        <div class="main-inner">

                <div class="content">

    <div class="document-title" style="margin-bottom:0px;">
        <h1>Search for Venues & Events Professionals</h1>
        <div class="text-center custom-white" style="z-index:10000000;">Making your events better with quality and proven event professionals.</div>


    </div><!-- /.document-title -->



                    <form class="filter" method="get" action="" id="form1">
    <div class="row">
        <div class="col-sm-12  col-md-offset-1 col-md-3">
            <div class="form-group">
                <input type="text" name="keyword" placeholder="Keyword" class="form-control" value="{!! isset($data['inputs']) ? $data['inputs']->keyword: '' !!}">
            </div><!-- /.form-group -->
        </div><!-- /.col-* -->

        <div class="col-sm-12 col-md-3">
            <div class="form-group">
                <input type="text" class="form-control" value="{!! isset($data['inputs']->address)? $data['inputs']->address: '' !!}" name="address" id="address" >

                <input type="hidden" value="{!! isset($data['inputs']->longitude)? $data['inputs']->longitude: '' !!}" name="longitude" id="long">
                <input type="hidden" name="latitude"  value="{!! isset($data['inputs']->latitude)? $data['inputs']->latitude: '' !!}"  id="lat">

            </div><!-- /.form-group -->
        </div><!-- /.col-* -->

        <div class="col-sm-12 col-md-3">
            <div class="form-group">
                    <select  name="category" class="form-control" title="Category">
                    <option value="all" selected="">All</option>
                        @foreach($professionals['data'] as $item)
                            <option value="{!! $item['_id']['$id'] !!}">{!! $item['name'] !!}</option>
                        @endforeach
                </select>
            </div><!-- /.form-group -->
        </div><!-- /.col-* -->


        <div class="col-sm-12 col-md-2">
            <button type="submit" class="btn btn-primary pull-left"><i class="fa fa-search fa-fw"></i> Search</button>
        </div><!-- /.col-* -->


    </div><!-- /.row -->
</form>
                    <script type="text/javascript">
                        //Autocomplete variables
                        var input = document.getElementById('address');
                        var lon2 = document.getElementById('long');
                        var lat2 = document.getElementById('lat');
                        var searchform = document.getElementById('form1');
                        var place;
                        var autocomplete = new google.maps.places.Autocomplete(input);

                        //Google Map variables
                        var map;
                        var marker;

                        //Add listener to detect autocomplete selection
                        google.maps.event.addListener(autocomplete, 'place_changed', function () {
                            place = autocomplete.getPlace();
                            console.log(place.geometry.location.lat());
                            lat2.value = place.geometry.location.lat();
                            lon2.value = place.geometry.location.lng();
                        });
                       /*
                        //Add listener to search
                        searchform.addEventListener("submit", function() {
                            var newlatlong = new google.maps.LatLng(place.geometry.location.lat(),place.geometry.location.lng());
                            map.setCenter(newlatlong);
                            marker.setPosition(newlatlong);
                            map.setZoom(12);

                        });
                        */

                        //Reset the inpout box on click
                        input.addEventListener('click', function(){
                            input.value = "";

                        });



                        function initialize() {
                            var myLatlng = new google.maps.LatLng(51.517503,-0.133896);
                            var mapOptions = {
                                zoom: 1,
                                center: myLatlng
                            }
                            map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

                            marker = new google.maps.Marker({
                                position: myLatlng,
                                map: map,
                                title: 'Main map'
                            });
                        }

                        google.maps.event.addDomListener(window, 'load', initialize);

                    </script>

                    <div class="container">
<h2 class="page-title">
    @if(isset($data))
        @if(isset($data['success']) && !$data['success'])
                {!! $data['data'] !!}
        @else

    {!! count($data['data']) !!} results matching your query

    <form method="get" action="http://preview.byaviators.com/template/superlist/listing-grid.html?" class="filter-sort">
        <div class="form-group">
            <select title="Sort by">
                <option name="price">Location</option>
                <option name="rating">Rating</option>
                <option name="title">Ratings</option>
            </select>
        </div><!-- /.form-group -->
    </form>
</h2><!-- /.page-title -->

<div class="cards-simple-wrapper">
    <div class="row">
            @foreach($data['data'] as $sp)
                @if($sp['status'] == 0)
            <div class="col-sm-6 col-md-4 col-lg-3" style="background-image: url('{!! isset($sp['background']) ? $sp['background'] : 'assets/img/tmp/product-1.jpg' !!}');">
                <div class="card-simple" data-background-image="{!! isset($sp['background']) ? $sp['background'] : 'assets/img/tmp/product-1.jpg' !!}">
                    <div class="card-simple-background">
                        <div class="card-simple-content">
                            <h2><a href="provider/{!! $sp['link'] !!}">{!! $sp['bizname'] !!} </a></h2>
                            <div class="card-simple-rating">
                                <i class="fa fa-star"></i>
                                @for($i=0; $i < ($sp['ratings'] - 1); $i++)
                                <i class="fa fa-star"></i>
                                    @endfor
                            </div><!-- /.card-rating -->

                            <div class="card-simple-actions">
                                <a href="#" class="fa fa-star-o"></a>
                                <a href="provider/{!! $sp['link'] !!}" class="fa fa-search"></a>
                                <a href="#" class="fa fa-heart-o link2"  onclick="giveHeart('{!! $sp['_id']['$id'] !!}')"></a>
                            </div><!-- /.card-simple-actions -->
                        </div><!-- /.card-simple-content -->
                      <br />
                        <div class="card-simple-label">{!! $sp['category'] !!}</div>

                            {!! ($sp['verified'] == 0) ? '<div class="card-simple-price" style="background-color: red">Unverified</div>' : '<div class="card-simple-price" style="background-color: green">Verified</div>' !!}

                    </div><!-- /.card-simple-background -->
                </div><!-- /.card-simple -->
            </div><!-- /.col-* -->
            @endif
            @endforeach


    </div><!-- /.row -->
</div><!-- /.cards-simple-wrapper -->


<div class="pager">
    <ul>
        <li><a href="#">Prev</a></li>
        <li><a href="#">5</a></li>
        <li class="active"><a>6</a></li>
        <li><a href="#">7</a></li>
        <li><a href="#">Next</a></li>
    </ul>
</div><!-- /.pagination -->
@endif
    @endif



                </div><!-- /.content -->
            </div><!-- /.container -->
        </div><!-- /.main-inner -->
    </div><!-- /.main -->
  @endsection