@extends('include.header')
@section('header_c')
    <div class="main">
        <div class="main-inner">
            <div class="container">
                <div class="content">
                    


                    <div class="row">
    <div class="col-sm-4 col-sm-offset-4" style="background:#fefefe;padding:20px;">
        <div class="page-title">
            <h3 class="text-center">Change Password</h3>
        </div><!-- /.page-title -->

        <form method="post" action="">
            @if(isset($data) && count($data) > 0 && isset($data['reason']))
                <div class="">
                    {!! $data['reason'] !!}
                </div>
            @endif
            {!! csrf_field() !!}
            <div class="form-group">
                <label for="login-form-email">New Password: </label>
                <input type="password" class="form-control" name="password" value="{!! isset($data['inputs'])? $data['inputs']->password:'' !!}" id="login-form-email">
            </div><!-- /.form-group -->

            <div class="form-group">
                <label for="login-form-email">Re: New Password: </label>
                <input type="password" class="form-control" name="rpassword" value="{!! isset($data['inputs'])? $data['inputs']->password:'' !!}" id="login-form-email">
            </div><!-- /.form-group -->

            <button type="submit" class="btn btn-primary btn-block text-center">Reset Password</button>
        </form>
    </div><!-- /.col-sm-4 -->
</div><!-- /.row -->

                </div><!-- /.content -->
            </div><!-- /.container -->
        </div><!-- /.main-inner -->
    </div><!-- /.main -->
@endsection