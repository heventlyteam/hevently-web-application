@extends('include.header')
@section('header_c')
    <div class="main">
        <div class="main-inner">
            <div class="container">
                <div class="content">
                    


                    <div class="row">
    <div class="col-sm-4 col-sm-offset-4" style="background:#fefefe;padding:2% ">
        	<div align="center">
				<div class="container-fluid bg-1 text-center">
  					<img src="warning.png" class="" style="display: inline; width: 40px; height: 40px;" alt="Bird">
                      <h2> <span class="text-warning">Fatal Err!</span></h2>
 					 <img src="apps_winamp.png" class="img-circle" alt="Bird">  
                      <h4><span class="text-danger">{!! $data !!}</span></h4>
				</div>
			</div>
    </div><!-- /.col-sm-4 -->
</div><!-- /.row -->

                </div><!-- /.content -->
            </div><!-- /.container -->
        </div><!-- /.main-inner -->
    </div><!-- /.main -->
@endsection