@extends('include.header')
@section('header_c')
    @if(isset($data) && is_array($data))
        <div class="main">
            <div class="main-inner">
                <div class="container">
                    <div class="content">



                        <div class="row">
                            <div class="col-sm-4 col-sm-offset-4" style="background:#fefefe;padding:2% ">
                                <div align="center">
                                    <div class="container-fluid bg-1 text-center">
                                        <img src="right.jpg" class="" style="display: inline; width: 40px; height: 40px;" alt="Bird">
                                        <h2> <span class="text-success">{!! isset($data['title']) ? $data['title'] : 'Not Defined' !!}</span></h2>
                                        <img src="success1.jpg" class="img-circle" alt="Bird">
                                        <h3><span class="text-danger">{!! isset($data['danger']) ? $data['danger'] : 'Not Defined' !!}</span></h3>
                                        <h4><span class="text-info">{!!  isset($data['message']) ? $data['message'] : 'Not Defined' !!}</span></h4>
                                    </div>
                                </div>
                            </div><!-- /.col-sm-4 -->
                        </div><!-- /.row -->

                    </div><!-- /.content -->
                </div><!-- /.container -->
            </div><!-- /.main-inner -->
        </div><!-- /.main -->
    @else
        {!! $data !!}
    @endif
@endsection