@extends('include.header')
@section('header_c')
    <div class="main">
        <div class="main-inner">
            <div class="container">
                <div class="content">
                    


                    <div class="row">
    <div class="col-sm-4 col-sm-offset-4" style="background:#fefefe;padding:2%">
        <div class="page-title">
            <h1>Sign Up to Hevently</h1>
        </div><!-- /.page-title -->
        <form method="post" action="">
            @if(isset($data) && count($data) > 0)
                <div class="">
                    {!! $data['reason'] !!}
                </div>
            @endif
            
            
            {!! csrf_field() !!}
            <div class="form-group">
                <label for="login-form-email">E-mail</label>
                <input type="email" class="form-control" value="@if(isset($data))
                {{ $data['inputs']['email'] }}
                @endif
                " name="email" id="login-form-email">
            </div><!-- /.form-group -->

            <div class="form-group">
                @if(isset($data) && array_key_exists('username', $data['fields']))
                    {{ $data['fields']['username'] }}
                @endif
                <label for="login-form-first-name">Username</label>
                <input type="text" class="form-control" value="@if(isset($data))
{{ $data['inputs']['username'] }}
                @endif
                 "  name="username" id="login-form-first-name">
            </div><!-- /.form-group -->

            <div class="form-group">
                
                @if(isset($data) && array_key_exists('password', $data['fields']))
                    {{ $data['fields']['password'] }}
                @endif
                <label for="login-form-password">Password</label>
                <input type="password" class="form-control" name="password" value=""  id="login-form-password">
            </div><!-- /.form-group -->

            <div class="form-group">
                @if(isset($data) && array_key_exists('rpassword', $data['fields']))
                    {{ $data['fields']['rpassword'] }}
                @endif
                <label for="login-form-password-retype">Retype password</label>
                <input type="password" class="form-control" value=""  name="rpassword" id="login-form-password-retype">
            </div><!-- /.form-group -->
            <p>By signing up, I agree to Hevently <a href="#">Terms and Conditions</a>.</p>
            <button type="submit" class="btn btn-primary pull-right">Register</button>
            <div class="clearfix"></div>
            <hr />
            <p class="text-center">Already have an Account?</p>
            <div class="text-center"><a href="register.php">Login</a></div>
        </form>
    </div><!-- /.col-sm-4 -->
</div><!-- /.row -->

                </div><!-- /.content -->
            </div><!-- /.container -->
        </div><!-- /.main-inner -->
    </div><!-- /.main -->
@endsection