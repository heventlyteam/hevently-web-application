@extends('include.header')
@section('header_c')
    <div class="main">
        <div class="main-inner">
                <div class="content">
<div class="mt-80">
                <div class="hero-image">
    <div class="hero-image-inner" style="background-image: url('assets/img/tmp/services.jpg');">
        <div class="hero-image-content">
            <div class="container text-center">
                <h1 style="margin-bottom:20px;" class="text-center">Make your services popular</h1>
                <p style="margin-bottom:20px;" class="text-center">Get people to know and require your services, <br>increase sales and make more income.</p>
                
                <a href="provider/register" class="btn btn-default text-center btn-lg">Register Now!</a>
                </div>
            </div><!-- /.container -->
        </div><!-- /.hero-image-content -->
        </div>
        </div>

            <div class="container" style="margin-top:50px;">
              <div class="col-md-5">
                <img src="assets/img/tmp/graph.png" class="img-responsive">
              </div>
              <div class="col-md-6 col-md-offset-1">
              <section>
                  <h2 style="font-size:48px;">Increased Sales & Income</h2>
                  <p style="font-size:24px">Increase your customer base and let more people know about your services. Thereby increasing sales and through output.</p>
              </section>
              </div>
            </div>
            
            <div style="margin-top:50px;background:#fefefe;border-top:10px dotted #f3f3f3;">
            <div class="container" style="padding-top:50px;padding-bottom:50px;">
              <div class="col-md-5">
              <section>
                  <h2 style="font-size:48px;">Acquire More Customers</h2>
                  <p style="font-size:24px">Increase your customer base and let more people know about your services. Thereby increasing sales and through output.</p>
              </section>
              </div>
              <div class="col-md-6 col-md-offset-1">
                <img src="assets/img/tmp/customer.png" class="img-responsive">
              </div>
            </div>
            </div>


            <div style="background:#f6f6f6;border-top:10px dotted #fff;t">
              <div class="container text-center" style="padding-top:50px;padding-bottom:50px;" >
               <h1>Increase sales and Make your services popular today!</h1>
                <a href="provider/register" class="btn btn-lg btn-green">Register &raquo;</a>
              </div>

                </div><!-- /.content -->
            </div><!-- /.container -->
        </div><!-- /.main-inner -->
    </div><!-- /.main -->

@endsection