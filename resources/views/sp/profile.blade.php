@extends('include.header')
@section('header_c')
    <?php
            if(isset($infoFom))
             $providerInfo = $infoFom['provider']['data'];
            else
                $providerInfo = $data['data'];
            //GET ALL REVIEWS
            $providerInfo['link'];
            $getReviews = new \App\Http\Controllers\SPReview();
            $review = $getReviews->getreviews($providerInfo['link']);
            $reviews = json_decode($review, true);
            if(isset($review['data']))
                $reviewall = $reviews['data'];
            else
                $reviewall = $reviews;
    ?>

    <?php
    //check if the user/provider session exists
    $check = new \App\Http\Controllers\LoginSessionManagement();
    $checkUser = $check->validateLoginSession('userinfo');
    $checkProvider = $check->validateLoginSession('sp_info');
    $sessionInfo = new \Illuminate\Support\Facades\Session();
    $userInfo = '';
    if($checkUser)
        $userInfo = $sessionInfo::get('userinfo');
    if($checkProvider)
        $userInfo = $sessionInfo::get('sp_info');

    ?>
    <div class="main">
        <div class="main-inner">
            <div class="content">
                <div class="mt-80 mb80">
                    <div class="detail-banner" style="background-image: url({!! ($providerInfo['background'] != '') ? $providerInfo['background'] : 'assets/img/tmp/detail-banner-1.jpg' !!});">
                        <div class="container">
                            <div class="detail-banner-left">
                                <div class="detail-banner-info">
                                    <div class="detail-label text-capitalize">{!! $providerInfo['category'] !!}</div>
                                    {!! ($providerInfo['verified'] != 0) ? '<div class="detail-verified">Verified</div>' : '<div class="detail-verified" style="background-color: red;">unverified</div>' !!}
                                </div><!-- /.detail-banner-info -->

                                <h2 class="detail-title">
                                    {!! $providerInfo['bizname'] !!}
                                </h2>

                                <div class="detail-banner-address">
                                    <i class="fa fa-map-o"></i> {!! (isset($providerInfo['address'])) ? $providerInfo['address']: '' !!}
                                </div><!-- /.detail-banner-address -->
                                <?php
                                    $rating = isset($providerInfo['rating']) ? $providerInfo['rating'] : $providerInfo['ratings'];
                                    $intRating = (int)$rating;
                                    $floatRating = ($rating % 2 == 0) ? 0 : 5;
                                ?>
                                <div class="detail-banner-rating">
                                    @for($i=0; $i < $intRating; $i++)
                                        <i class="fa fa-star"></i>
                                    @endfor
                                    {!! ($floatRating > 0)? '<i class="fa fa-star-half-o"></i>' : '' !!}
                                </div><!-- /.detail-banner-rating -->

                                <div class="detail-banner-btn bookmark">
                                    <i class="fa fa-bookmark-o"></i> <span data-toggle="Bookmarked">Bookmark</span>
                                </div><!-- /.detail-claim -->
                                <a href="#" onclick="giveHeart('{!! $providerInfo['_id']['$id'] !!}')" class="link2">
                                    <div class="detail-banner-btn heart">
                                        <i class="fa fa-heart-o"></i><span class="heart" data-toggle="I Love It">Give Heart</span>
                                    </div><!-- /.detail-claim -->
                                </a>
                            </div><!-- /.detail-banner-left -->
                        </div><!-- /.container -->
                    </div><!-- /.detail-banner -->

                </div>

                <div class="container">
                    <div class="row detail-content">
                        <div class="col-sm-7">
                            <div class="detail-gallery">
                                <div class="detail-gallery-preview">
                                    <a href="assets/img/tmp/gallery-1.jpg">
                                        <img src="assets/img/tmp/gallery-1.jpg">
                                    </a>
                                </div>
                                @if(isset($providerInfo['other_images']))
                                <ul class="detail-gallery-index">
                                    <li class="detail-gallery-list-item active">
                                        <a data-target="assets/img/tmp/gallery-1.jpg">
                                            <img src="assets/img/tmp/gallery-1.jpg" alt="...">
                                        </a>
                                    </li>
                                </ul>
                                    @endif
                            </div><!-- /.detail-gallery -->

                            <h2>About <span class="text-secondary">{!! $providerInfo['bizname'] !!}</span></h2>
                            <div class="background-white p20">
                                <div class="detail-vcard">
                                    <div class="detail-logo">
                                        <img src="assets/img/tmp/pragmaticmates-logo.png">
                                    </div><!-- /.detail-logo -->

                                    <div class="detail-contact">
                                        <div class="detail-contact-email">
                                            <i class="fa fa-envelope-o"></i> <a href="mailto:{!! $providerInfo{'email'} !!}">{!! $providerInfo{'email'} !!}</a>
                                        </div>
                                        <div class="detail-contact-phone">
                                            <i class="fa fa-mobile-phone"></i> <a href="tel:#">{!! $providerInfo{'phone'} !!}</a>
                                        </div>
                                        <div class="detail-contact-website">
                                            <i class="fa fa-globe"></i> <a href="#">{!! $providerInfo{'website'} !!}</a>
                                        </div>
                                        <div class="detail-contact-address">
                                            <i class="fa fa-map-o"></i>
                                            {!! isset($providerInfo{'address'}) ? $providerInfo{'address'} : '' !!}
                                        </div>
                                    </div><!-- /.detail-contact -->
                                </div><!-- /.detail-vcard -->

                                <div class="detail-description">
                                    {!! isset($providerInfo{'description'}) ? $providerInfo{'description'} : $providerInfo['about'] !!}
                                </div>

                                <div class="detail-follow">
                                    <h5>Follow Us:</h5>
                                    <div class="follow-wrapper">
                                        <a class="follow-btn facebook" href="#"><i class="fa fa-facebook"></i></a>
                                        <a class="follow-btn youtube" href="#"><i class="fa fa-youtube"></i></a>
                                        <a class="follow-btn twitter" href="#"><i class="fa fa-twitter"></i></a>
                                        <a class="follow-btn tripadvisor" href="#"><i class="fa fa-tripadvisor"></i></a>
                                        <a class="follow-btn google-plus" href="#"><i class="fa fa-google-plus"></i></a>
                                    </div><!-- /.follow-wrapper -->
                                </div><!-- /.detail-follow -->
                            </div>
                            @if($providerInfo['video'] != '' && isset($providerInfo['video']))
                            <h2>Video</h2>
                            <div class="detail-video">
                                 <iframe src="{!! $providerInfo['video'] !!}" width="653" height="366" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                            </div><!-- /.detail-video -->
                            @endif
                            <h2 id="reviews">All Reviews</h2>
                            <div class="reviews">
                                @if(count($reviewall) == 0 || $reviewall['status'] == 'fail')
                                    <h3><span class="label label-warning">No review for {!! $providerInfo['bizname'] !!}</span></h3>
                                    @else
                                @foreach($reviewall as $rev)
                                <div class="review">
                                    <div class="review-image">
                                        <img src="assets/img/tmp/agent-1.jpg" alt="">
                                    </div><!-- /.review-image -->

                                    <div class="review-inner">
                                        <div class="review-title">
                                            <h2>Nancy Collins</h2>

                <span class="report">
                    <span class="separator">&#8226;</span><i class="fa fa-flag" title="Report" data-toggle="tooltip" data-placement="top"></i>
                </span>

                                            <div class="review-overall-rating">
                                                <span class="overall-rating-title">Total Score:</span>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star-half-o"></i>
                                                <i class="fa fa-star-o"></i>
                                            </div><!-- /.review-rating -->
                                        </div><!-- /.review-title -->

                                        <div class="review-content-wrapper">
                                            <div class="review-content">
                                                <div class="review-pros">
                                                    <p>Quisque aliquet ornare nunc in viverra. Nullam ornare molestie ligula in luctus. Suspendisse ac cursus elit. In congue mattis felis, non hendrerit orci dictum id.</p>
                                                </div><!-- /.pros -->
                                            </div><!-- /.review-content -->

                                            <div class="review-rating">
                                                <dl>
                                                    <dt>Food</dt>
                                                    <dd>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </dd>
                                                    <dt>Staff</dt>
                                                    <dd>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star-o"></i>
                                                    </dd>
                                                    <dt>Value</dt>
                                                    <dd>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star-o"></i>
                                                        <i class="fa fa-star-o"></i>
                                                    </dd>
                                                    <dt>Atmosphere</dt>
                                                    <dd>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star-o"></i>
                                                        <i class="fa fa-star-o"></i>
                                                    </dd>
                                                </dl>
                                            </div><!-- /.review-rating -->
                                        </div><!-- /.review-content-wrapper -->

                                    </div><!-- /.review-inner -->
                                </div><!-- /.review -->
                                    @endforeach
                                    @endif
                            </div><!-- /.reviews -->

                        </div><!-- /.col-sm-7 -->

                        <div class="col-sm-5">

                            <div class="background-white p20">
                                <div class="detail-overview-hearts">
                                    <i class="fa fa-heart"></i> <strong>{!! count($providerInfo['love']) !!} </strong>people love it
                                </div>
                                <div class="detail-overview-rating">
                                    <i class="fa fa-star"></i> <strong>{!! $providerInfo['ratings'] !!} / 5 </strong>from <a href="#reviews">316 reviews</a>
                                </div>

                                <div class="detail-actions row">
                                    <div class="col-sm-4">
                                        <div class="btn btn-secondary btn-share"><i class="fa fa-share-square-o"></i> Share
                                            <div class="share-wrapper">
                                                <ul class="share">
                                                    <li><i class="fa fa-facebook"></i> Facebook</li>
                                                    <li><i class="fa fa-twitter"></i> Twitter</li>
                                                    <li><i class="fa fa-google-plus"></i> Google+</li>
                                                    <li><i class="fa fa-pinterest"></i> Pinterest</li>
                                                    <li><i class="fa fa-chain"></i> Link</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div><!-- /.col-sm-4 -->
                                    <div class="col-sm-4">
                                        <a href="provider/{!! $providerInfo['link'] !!}/hire"><div class="btn btn-secondary btn-claim"><i class="fa fa-hand-peace-o"></i> Hire me</div></a>
                                    </div><!-- /.col-sm-4 -->
                                </div><!-- /.detail-actions -->
                            </div>

                        </div><!-- /.col-sm-5 -->

                        <div class="col-sm-12">
                            <h2>Submit a Review</h2>

                            <form class="background-white p20 add-review" method="post" action="#">
                                {!! csrf_field() !!}
                                <input type="hidden" name="provider" value="{!! $providerInfo['_id']['$id'] !!}" />
                                <div class="row">
                                    <div class="form-group col-sm-6">
                                        <label for="">Name <span class="required">*</span></label>
                                        <input type="text" class="form-control" name="name" id="" {!! ($checkUser) ? 'value="'.$userInfo->username.'" disabled':'required' !!}>
                                    </div><!-- /.col-sm-6 -->

                                    <div class="form-group col-sm-6">
                                        <label for="">Email <span class="required">*</span></label>
                                        <input type="email" class="form-control" name="email" id="" {!! ($checkUser) ? 'value="'.$userInfo->email.'" disabled':'required' !!}>
                                    </div><!-- /.col-sm-6 -->
                                </div><!-- /.row -->

                                <div class="row">
                                    <div class="form-group input-rating col-sm-3">

                                        <div class="rating-title">Service</div>

                                        <input type="radio" value="1" name="service" id="rating-food-1">
                                        <label for="rating-food-1"></label>
                                        <input type="radio" value="2" name="service" id="rating-food-2">
                                        <label for="rating-food-2"></label>
                                        <input type="radio" value="3" name="service" id="rating-food-3">
                                        <label for="rating-food-3"></label>
                                        <input type="radio" value="4" name="service" id="rating-food-4">
                                        <label for="rating-food-4"></label>
                                        <input type="radio" value="5" name="service" id="rating-food-5">
                                        <label for="rating-food-5"></label>

                                    </div><!-- /.col-sm-3 -->
                                    <div class="form-group input-rating col-sm-3">

                                            <div class="rating-title">Attendance</div>

                                        <input type="radio" value="1" name="attendance" id="rating-staff-1">
                                        <label for="rating-staff-1"></label>
                                        <input type="radio" value="2" name="attendance" id="rating-staff-2">
                                        <label for="rating-staff-2"></label>
                                        <input type="radio" value="3" name="attendance" id="rating-staff-3">
                                        <label for="rating-staff-3"></label>
                                        <input type="radio" value="4" name="attendance" id="rating-staff-4">
                                        <label for="rating-staff-4"></label>
                                        <input type="radio" value="5" name="attendance" id="rating-staff-5">
                                        <label for="rating-staff-5"></label>

                                    </div><!-- /.col-sm-3 -->
                                    <div class="form-group input-rating col-sm-3">

                                        <div class="rating-title">Punctuality</div>

                                        <input type="radio" value="1" name="punctuality" id="rating-value-1">
                                        <label for="rating-value-1"></label>
                                        <input type="radio" value="2" name="punctuality" id="rating-value-2">
                                        <label for="rating-value-2"></label>
                                        <input type="radio" value="3" name="punctuality" id="rating-value-3">
                                        <label for="rating-value-3"></label>
                                        <input type="radio" value="4" name="punctuality" id="rating-value-4">
                                        <label for="rating-value-4"></label>
                                        <input type="radio" value="5" name="punctuality" id="rating-value-5">
                                        <label for="rating-value-5"></label>

                                    </div><!-- /.col-sm-3 -->
                                    <div class="form-group input-rating col-sm-3">

                                        <div class="rating-title">Overall Quality</div>

                                        <input type="radio" value="1" name="quality" id="rating-atmosphere-1">
                                        <label for="rating-atmosphere-1"></label>
                                        <input type="radio" value="2" name="quality" id="rating-atmosphere-2">
                                        <label for="rating-atmosphere-2"></label>
                                        <input type="radio" value="3" name="quality" id="rating-atmosphere-3">
                                        <label for="rating-atmosphere-3"></label>
                                        <input type="radio" value="4" name="quality" id="rating-atmosphere-4">
                                        <label for="rating-atmosphere-4"></label>
                                        <input type="radio" value="5" name="quality" id="rating-atmosphere-5">
                                        <label for="rating-atmosphere-5"></label>

                                    </div><!-- /.col-sm-3 -->
                                </div><!-- /.row -->

                                <div class="row">
                                    <div class="form-group col-sm-12">
                                        <label for="">Comment <span class="required">*</span></label>
                                        <textarea class="form-control" name="comment" rows="5" id=""></textarea>
                                    </div><!-- /.col-sm-6 -->
                                    <div class="col-sm-8">
                                        <p>Required fields are marked <span class="required">*</span></p>
                                    </div><!-- /.col-sm-8 -->
                                    <div class="col-sm-4">
                                        <button class="btn btn-primary btn-block" type="submit" name="submit_review"><i class="fa fa-star"></i>Submit Review</button>
                                    </div><!-- /.col-sm-4 -->
                                </div><!-- /.row -->
                            </form>
                        </div><!-- /.col-* -->
                    </div><!-- /.row -->

                </div><!-- /.container -->

            </div><!-- /.content -->
        </div><!-- /.main-inner -->
    </div><!-- /.main -->

@endsection