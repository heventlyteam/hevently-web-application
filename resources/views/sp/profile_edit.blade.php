<div class="load_main">
    <div class="row">
        <div class="col-lg-6">
            <h1>Profile Edit <span class="text text-danger">(Temporarily unavailable)</span></h1>
            <span class="text text-info">Please provide valid and true data about yourself and business</span>
            <hr>
            <div class="">
                <form class="form" method="post" action="#" onsubmit="return false;">
                    <div class="form-group">
                        <label class="label-control">Username (<span class="text text-justify">This cannot be edited</span>) :</label>
                        <input type="text" disabled value="username" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label for="login-form-email">Business Name</label>
                        <input type="text" class="form-control" name="bizname" value="" id="login-form-name" pmbx_context="FFBF86E6-E8A5-439E-8892-D152E97CA8BE">
                    </div>
                    <div class="form-group">
                        <label for="login-form-first-name">About Your Business</label>
                        <textarea rows="5" class="form-control" name="about_biz" placeholder="Maximum of 150 characters"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="login-form-last-name">Address</label>
                        <input type="text" class="form-control" value="" name="address" id="address" placeholder="Enter a location" autocomplete="off" pmbx_context="5E48FBAD-7A03-4427-B194-AF6D8984177B">

                        <input type="hidden" value="" name="longitude" id="long">
                        <input type="hidden" name="latitude" value="" id="lat" />
                    </div>
            </div>
        </div>


        <div class="col-lg-6">
            <h1>Profile Edit <span class="text text-success">(Available)</span></h1>
            <span class="text text-info">Please provide valid and true data about yourself and business</span>
            <hr>
            <div class="">
                 <div class="form-group">
                        <label class="label-control">Display Video link (<span class="text text-justify">e.g http://www.youtube.com/...</span>) :</label>
                        <input type="text" value="" name="video_link" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label for="login-form-email">Facebook Link</label>
                        <input type="text" class="form-control" name="fb_link" value="" id="login-form-name" pmbx_context="FFBF86E6-E8A5-439E-8892-D152E97CA8BE">
                    </div>
                    <div class="form-group">
                        <label for="login-form-first-name">Your Personal Website</label>
                        <input type="text" class="form-control" name="personal_web" value="" id="login-form-name" pmbx_context="FFBF86E6-E8A5-439E-8892-D152E97CA8BE">

                    </div>
                    <div class="form-group">
                        <label for="login-form-last-name">Phone Number</label>
                        <input type="text" class="form-control" value="" name="p_num" id="address" placeholder="Enter your business line" pmbx_context="5E48FBAD-7A03-4427-B194-AF6D8984177B">
                    </div>
                    {!! csrf_field() !!}
                </div>
            </div>
        </div>
        <div class="form-group">
            <button id="submit" class="form-control" onclick="editProfile()">Edit...</button>
            <script>

            </script>
        </div>
    </div>
</div>