<div class="load_main">
    <h1>Change Password</h1>
    <span class="text text-info">Strong password required</span>
    <span class="text text-warning">Oops! could not start password strength checker</span>
    <hr>
    <span class="text-danger" style="color: red;">
        {!! isset($data['error']) ? $data['error'] : '' !!}
        {!! isset($data['message']) ? $data['message'] : '' !!}
    </span>
    <div class="">
        <form class="form" method="post" action="#" onsubmit="return false;" enctype="multipart/form-data">
            <div class="form-group">
                <label class="label-control">Username (<span class="text text-justify">This cannot be edited</span>) :</label>
                <input type="text" disabled value="username" class="form-control" />
            </div>
            <div class="form-group">
                <label for="login-form-email">Prev Password</label>
                <input type="password" class="form-control" name="old_password" value="" id="login-form-name">
            </div>
            <div class="form-group">
                <label for="login-form-first-name">New Password(atleast 7chars)</label>
                <input type="password" class="form-control" name="new_password1" value="" id="login-form-name">
            </div>
            <div class="form-group">
                <label for="login-form-last-name">Password confirmation: </label>

                <input type="password" class="form-control" name="new_password2" value="" id="login-form-name">
            </div>
            {!! csrf_field() !!}
            <div class="form-group">
                <button id="submit" class="form-control" onclick="changePassword()">Edit...</button>
                <script>

                </script>
            </div>
        </form>

    </div>
</div>