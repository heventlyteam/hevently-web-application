@extends('include.header')
@section('header_c')
    <?php
        $getProfessionanls = new \App\Http\Controllers\ProfessionalsController();
        $professional = $getProfessionanls->getProfessionals();
        $pro = json_decode($professional, true);
    ?>
    <div class="main">
        <div class="main-inner">
            <div class="container">
                <div class="content">
                    


                    <div class="row">
    <div class="col-sm-4 col-sm-offset-4">
        <div class="page-title">
            <h1 align="center">Register as a Service Provider</h1>
        </div><!-- /.page-title -->

        <form method="post" action="" name="form1">
            @if(isset($data) && count($data) > 0)
                <div class="">
                    {!! $data['reason'] !!}
                </div>
            @endif
            
            {!! csrf_field() !!}
            <div class="form-group">
                <label for="">Category</label>
                <select title="Category" name="category">
                    @foreach($pro['data'] as $pr)
                        <option value="{!! $pr['name'] !!}">{!! $pr['name'] !!}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="login-form-email">Business Name</label>
                <input type="text" class="form-control" name="bizname" value="{!! isset($data['inputs']->bizname)? $data['inputs']->bizname: '' !!}" id="login-form-name">
            </div><!-- /.form-group -->

            <div class="form-group">
                <label for="login-form-first-name">About Your Business</label>
                <textarea rows="5" class="form-control" name="about" placeholder="Maximum of 150 characters">{!! isset($data['inputs']->about)? $data['inputs']->about: '' !!}</textarea>
            </div><!-- /.form-group -->

            <div class="form-group">
                <label for="login-form-last-name">Address</label>
                <input type="text" class="form-control" value="{!! isset($data['inputs']->address)? $data['inputs']->address: '' !!}" name="address" id="address" >
                
                <input type="hidden" value="{!! isset($data['inputs']->longitude)? $data['inputs']->longitude: '' !!}" name="longitude" id="long">
                <input type="hidden" name="latitude"  value="{!! isset($data['inputs']->latitude)? $data['inputs']->latitude: '' !!}"  id="lat">
            </div><!-- /.form-group -->
            <div class="form-group">
                <label for="login-form-last-name">Email</label>
                <input type="text" class="form-control" name="email"  value="{!! isset($data['inputs']->email)? $data['inputs']->email: '' !!}"  id="login-form-last-name">
            </div><!-- /.form-group -->

            <div class="form-group">
                <label for="login-form-password">Username</label>
                <input type="text" class="form-control" value="{!! isset($data['inputs']->username)? $data['inputs']->username: '' !!}" name="username" id="login-form-password">
            </div><!-- /.form-group -->


            <div class="form-group">
                <label for="login-form-password">Password</label>
                <input type="password" class="form-control" name="password" id="login-form-password">
            </div><!-- /.form-group -->

            <div class="form-group">
                <label for="login-form-password-retype">Retype password</label>
                <input type="password" class="form-control" name="rpassword" id="login-form-password-retype">
            </div><!-- /.form-group -->

            <button type="submit" class="btn btn-primary pull-right">Register</button>
        </form>
        <script type="text/javascript">
    //Autocomplete variables
    var input = document.getElementById('address');
    var lon2 = document.getElementById('long');
    var lat2 = document.getElementById('lat');
    var searchform = document.getElementById('form1');
    var place;
    var autocomplete = new google.maps.places.Autocomplete(input);
 
    //Google Map variables
    var map;
    var marker;
 
    //Add listener to detect autocomplete selection
    google.maps.event.addListener(autocomplete, 'place_changed', function () {
        place = autocomplete.getPlace();
        console.log(place.geometry.location.lat());
        lat2.value = place.geometry.location.lat();
        lon2.value = place.geometry.location.lng();
    });
    //Add listener to search
    searchform.addEventListener("submit", function() {
        var newlatlong = new google.maps.LatLng(place.geometry.location.lat(),place.geometry.location.lng());
        map.setCenter(newlatlong);
        marker.setPosition(newlatlong);
        map.setZoom(12);
 
    });
     
    //Reset the inpout box on click
    input.addEventListener('click', function(){
        input.value = "";
        
    });
 
 
 
    function initialize() {
      var myLatlng = new google.maps.LatLng(51.517503,-0.133896);
      var mapOptions = {
        zoom: 1,
        center: myLatlng
      }
      map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
 
      marker = new google.maps.Marker({
          position: myLatlng,
          map: map,
          title: 'Main map'
      });
    }
 
    google.maps.event.addDomListener(window, 'load', initialize);
 
</script>
    </div><!-- /.col-sm-4 -->
</div><!-- /.row -->

                </div><!-- /.content -->
            </div><!-- /.container -->
        </div><!-- /.main-inner -->
    </div><!-- /.main -->

@endsection
