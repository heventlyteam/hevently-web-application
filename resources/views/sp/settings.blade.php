<div class="load_main">
    <h1>Account Settings</h1>
    <span class="text text-info"></span>
    <hr>
    <div class="">
        <div class="list-group">
            <a href="#user-profile-edit" class="list-group-item">Edit Profile</a>
            <a href="#sp/view/dp" class="list-group-item">Change Password</a>
            <a href="#sp/cngpassword" class="list-group-item">View/Edit Display Pictures</a>
            <a href="provider/logout" class="list-group-item">Logout</a>
        </div>
    </div>
</div>