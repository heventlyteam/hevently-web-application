<?php
    if(isset($data))
        $dataD = json_decode($data, true)
?>
<div class="load_main">
    <h1>Display Pictures</h1>
    <span class="text text-info">You may not be able to upload more than 10 images</span>
    <div class="col-lg-12">
        <form method="POST" action="sp/upload" class="upload_imag2e" enctype="multipart/form-data">
            <div class="form-group">
                <label>Upload a Background <Image></Image></label>
                <input type="file" name="background" placeholder="Upload a Background Image" />
               </div>
            {!! csrf_field() !!}
           <div class="form-group">
                <label>Upload a thumbnail</label>
                <input type="file" name="image" placeholder="Upload a thumbnail" />
            </div>
            {!! csrf_field() !!}

            <div class="form-group">
                <input type="submit" name="submit" class="upload_image_" value="+Image" />
            </div>
        </form>
        <script type="text/javascript">
            $('form.upload_image').submit(function(e){
                e.preventDefault();
                upload_image($('form.upload_image').serialize());
            });
        </script>
    </div>
    <hr>
    <div>
        <!-- list of all images -if ther exist-->
        <h1>Other Pictures</h1>
        <span class="text text-info">You may not be able to upload more than 10 images</span>
        <hr>
        <div class="col-lg-12">
            @if(isset($dataD['data']['images']) && count($dataD['data']['images']))
                @foreach($dataD['data']['images'] as $image)
                    <div class="col-lg-4 each-image">
                        <img src="{!! $image !!}" class="" />
                        <div class="delete_btn"><a href="#delete">delete</a></div>
                    </div>
                @endforeach
            @else
                <p>You have no uploaded images yet.</p>
            @endif
        </div>

        <h1>Banners</h1>
        <span class="text text-info">You may not be able to upload more than 10 images</span>
        <hr>
        @if(isset($dataD['data']['background']) && count($dataD['data']['background']))

        <div class="col-lg-12">
                <img src="{!! $dataD['data']['background'] !!}" class="banner-img" />
                <div class="delete_btn"><a href="#delete">delete</a></div>

            </div>
        @endif
            </div>
    </div>
</div>