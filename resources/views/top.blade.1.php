@extends('include.footer')
@section('whol')
<!DOCTYPE html>
<html>
<head>
    <base url="http://localhost:8000/" href="http://localhost:8000/" />
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    
    <link href="assets/libraries/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="assets/libraries/owl.carousel/assets/owl.carousel.css" rel="stylesheet" type="text/css" >
    <link href="assets/libraries/colorbox/example1/colorbox.css" rel="stylesheet" type="text/css" >
    <link href="assets/libraries/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" type="text/css">
    <link href="assets/libraries/bootstrap-fileinput/fileinput.min.css" rel="stylesheet" type="text/css">
    <link href="assets/css/superlist.css" rel="stylesheet" type="text/css" >
    <link rel="stylesheet" type="text/css" href="assets/css/cavve.css">

    <link rel="shortcut icon" type="image/x-icon" href="assets/favicon.png">

    <title>Hevently - Home</title>
</head>


<body>

<div class="page-wrapper">
    @yield('top_content')
  
 @endsection