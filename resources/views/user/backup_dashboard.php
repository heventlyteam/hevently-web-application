@include("include/user/top")
    
    <header class="header header-minimal">
    <div class="header-wrapper">
        <div class="container-fluid">
            <div class="header-inner">
                <div class="header-logo">
                    <a href="index-2.html">
                        <img src="../assets/img/logo.png" alt="Logo">
                    </a>
                </div><!-- /.header-logo -->

                <div class="header-content">
                    <div class="header-bottom">
       

<button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target=".header-nav-primary">
    <span class="sr-only">Toggle navigation</span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
</button>


                        <div class="header-nav-user">
    <div class="dropdown">
        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
            <div class="user-image">
                <img src="../assets/img/tmp/agent-2.jpg">
                <div class="notification"></div><!-- /.notification-->
            </div><!-- /.user-image -->

            <span class="header-nav-user-name">Natasha Samson</span> <i class="fa fa-chevron-down"></i>
        </button>

        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
            <li><a href="user-profile-edit.html">Edit Profile</a></li>
            <li><a href="listing-submit.html">Submit Listing</a></li>
            <li><a href="change-password.html">Change Password</a></li>
        </ul>
    </div><!-- /.dropdown -->
</div><!-- /.header-nav-user -->

                    </div><!-- /.header-bottom -->
                </div><!-- /.header-content -->
            </div><!-- /.header-inner -->
        </div><!-- /.container -->
    </div><!-- /.header-wrapper -->

    <div class="header-statusbar" style="padding:18px;">
        <div class="header-statusbar-inner" style="padding:0px;">
            <div class="header-statusbar-left">
                <h1>Dashboard</h1>                
            </div><!-- /.header-statusbar-left -->

            <div class="header-statusbar-right">
                <div class="hidden-xs visible-lg">
                    Navigation:
                    <ul class="breadcrumb">
                        <li><a href="#">Hevently</a></li>
                        <li><a href="#">Dashboard</a></li>
                    </ul>
                </div>
            </div><!-- /.header-statusbar-right -->
        </div><!-- /.header-statusbar-inner -->
    </div><!-- /.header-statusbar -->
</header><!-- /.header -->




    <div class="main">
        <div class="outer-admin">
            <div class="wrapper-admin">
                @include("include.user.sidebar-primary")
                <div class="content-admin">
                    <div class="content-admin-wrapper">
                        <div class="content-admin-main">
                            <div class="content-admin-main-inner">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-sm-12">
    <div class="row">
        <div class="col-sm-12">
            <div class="col-md-3">
              <div class="custom-short-box" style="background-color:#f00;padding:10px;margin:0px;">

                <div class="col-md-6">
                    <div style="font-size:60px;margin:0px;padding:0px;color:#fefefe;"><i class="fa fa-inbox fa-fw"></i></div>
                </div>
                <div class="col-md-6">
                  <div style="color:#fefefe;">
                      <h5>Messages</h5>
                      <h1>45</h1>
                  </div>
                </div>
              <div class="clearfix"></div>

              </div>
            </div>
            <div class="col-md-3">
            </div>
        </div><!-- /.col-* -->
    </div><!-- /.row -->

    <div class="row">
        <div class="col-sm-12 col-lg-6">
            <h3>Current Balance Status</h3>

            <div class="row">
                <div class="col-sm-6">
                    <div class="statusbox">
                        <h2>Balance</h2>
                        <div class="statusbox-content">
                            <strong>$25,000</strong>
                            <span>Updated 27/04/2015</span>
                        </div><!-- /.statusbox-content -->

                        <div class="statusbox-actions">
                            <a href="#"><i class="fa fa-eye"></i></a>
                            <a href="#"><i class="fa fa-bar-chart"></i></a>
                            <a href="#"><i class="fa fa-share-alt"></i></a>
                        </div><!-- /.statusbox-actions -->
                    </div><!-- /.statusbox -->
                </div>

                <div class="col-sm-6">
                    <div class="statusbox">
                        <h2>Progress</h2>
                        <div class="statusbox-content">
                            <strong>$13,200</strong>
                            <span>Updated 27/04/2015</span>
                        </div><!-- /.statusbox-content -->

                        <div class="statusbox-actions">
                            <a href="#"><i class="fa fa-eye"></i></a>
                            <a href="#"><i class="fa fa-bar-chart"></i></a>
                            <a href="#"><i class="fa fa-share-alt"></i></a>
                        </div><!-- /.statusbox-actions -->
                    </div>
                </div>
            </div><!-- /.row -->
        </div><!-- /.col-* -->

        <div class="col-sm-12 col-lg-6">
            <h3>Monthly Expenses</h3>

            <div class="row">
                <div class="col-sm-6">
                    <div class="statusbox">
                        <h2>Payments</h2>
                        <div class="statusbox-content">
                            <strong>$14,800</strong>
                            <span>Updated 27/04/2015</span>
                        </div><!-- /.statusbox-content -->

                        <div class="statusbox-actions">
                            <a href="#"><i class="fa fa-eye"></i></a>
                            <a href="#"><i class="fa fa-bar-chart"></i></a>
                            <a href="#"><i class="fa fa-share-alt"></i></a>
                        </div><!-- /.statusbox-actions -->
                    </div><!-- /.statusbox -->
                </div><!-- /.col-* -->

                <div class="col-sm-6">
                    <div class="statusbox">
                        <h2>Avg. Salary</h2>
                        <div class="statusbox-content">
                            <strong>$12,800</strong>
                            <span>Updated 27/04/2015</span>
                        </div><!-- /.statusbox-content -->

                        <div class="statusbox-actions">
                            <a href="#"><i class="fa fa-eye"></i></a>
                            <a href="#"><i class="fa fa-bar-chart"></i></a>
                            <a href="#"><i class="fa fa-share-alt"></i></a>
                        </div><!-- /.statusbox-actions -->
                    </div><!-- /.statusbox -->
                </div><!-- /.col-* -->
            </div><!-- /.row -->
        </div><!-- /.col-* -->
    </div><!-- /.row -->

    <div class="row">
        <div class="col-sm-12">
            <h3>Sales &amp; Expenses Comparison Chart</h3>
            <div class="p30 background-white mb50">
                <div id="superlist-chart"></div>
            </div>
        </div><!-- /.col-* -->
    </div><!-- /.row -->

    <div class="row">
        <div class="col-sm-12 col-lg-5">
            <h3>Recent Users <a href="#">View All Logs <i class="fa fa-chevron-right"></i></a></h3>

            <div class="users">
    <table class="table">
        <tbody>
            <tr>
                <td><a class="user" href="#"><img src="../assets/img/tmp/agent-1.jpg" alt=""></a></td>
                <td class="hidden-xs visible-sm visible-md visible-lg">
                    <h2><a href="#">Fiona Wilson</a></h2>
                    <h3>Last login: 28/07/1015 10:45</h3>
                </td>
                <td class="right">
                    <a href="#" class="btn btn-xs btn-primary">Edit</a>
                    <a href="#" class="btn btn-xs btn-danger">Remove</a>
                </td>
            </tr>

            <tr>
                <td><a class="user" href="#"><img src="../assets/img/tmp/agent-2.jpg" alt=""></a></td>
                <td class="hidden-xs visible-sm visible-md visible-lg">
                    <h2><a href="#">Natasha Samson</a></h2>
                    <h3>Last login: 28/07/1015 10:45</h3>
                </td>
                <td class="right">
                    <a href="#" class="btn btn-xs btn-primary">Edit</a>
                    <a href="#" class="btn btn-xs btn-danger">Remove</a>
                </td>
            </tr>

            <tr>
                <td><a class="user" href="#"><img src="../assets/img/tmp/agent-3.jpg" alt=""></a></td>
                <td class="hidden-xs visible-sm visible-md visible-lg">
                    <h2><a href="#">Kim Glove</a></h2>
                    <h3>Last login: 28/07/1015 10:45</h3>
                </td>
                <td class="right">
                    <a href="#" class="btn btn-xs btn-primary">Edit</a>
                    <a href="#" class="btn btn-xs btn-danger">Remove</a>
                </td>
            </tr>

            <tr>
                <td><a class="user" href="#"><img src="../assets/img/tmp/agent-4.jpg" alt=""></a></td>
                <td class="hidden-xs visible-sm visible-md visible-lg">
                    <h2><a href="#">Richard Peterson</a></h2>
                    <h3>Last login: 28/07/1015 10:45</h3>
                </td>
                <td class="right">
                    <a href="#" class="btn btn-xs btn-primary">Edit</a>
                    <a href="#" class="btn btn-xs btn-danger">Remove</a>
                </td>
            </tr>

            <tr>
                <td><a class="user" href="#"><img src="../assets/img/tmp/agent-5.jpg" alt=""></a></td>
                <td class="hidden-xs visible-sm visible-md visible-lg">
                    <h2><a href="#">Rachel Fast</a></h2>
                    <h3>Last login: 28/07/1015 10:45</h3>
                </td>
                <td class="right">
                    <a href="#" class="btn btn-xs btn-primary">Edit</a>
                    <a href="#" class="btn btn-xs btn-danger">Remove</a>
                </td>
            </tr>
        </tbody>
    </table>
</div><!-- /.users -->

        </div><!-- /.col-* -->

        <div class="col-sm-12 col-lg-7">
            <h3>New Listings <a href="#">All Pending Listings <i class="fa fa-chevron-right"></i></a></h3>

            <div class="background-white p30 mb50">
    <div class="cards-system">
        

        
            
            <div class="card-system">
                <div class="card-system-inner">
                    <div class="card-system-image" data-background-image="../assets/img/tmp/product-2.jpg">
                        <a href="listing-detail.html"></a>
                    </div><!-- /.card-system-image -->

                    <div class="card-system-content">
                        <h2><a href="listing-detail.html">Tasty Brazil Coffee</a></h2>
                        <h3>Posted sever hours ago</h3>
                        <a href="#" class="btn btn-primary btn-xs">Edit</a>
                        <a href="#" class="btn btn-secondary btn-xs">Ban</a>
                    </div>
                </div>
            </div><!-- /.card-system -->
        
            
            <div class="card-system">
                <div class="card-system-inner">
                    <div class="card-system-image" data-background-image="../assets/img/tmp/product-3.jpg">
                        <a href="listing-detail.html"></a>
                    </div><!-- /.card-system-image -->

                    <div class="card-system-content">
                        <h2><a href="listing-detail.html">Healthy Breakfast</a></h2>
                        <h3>Posted sever hours ago</h3>
                        <a href="#" class="btn btn-primary btn-xs">Edit</a>
                        <a href="#" class="btn btn-secondary btn-xs">Ban</a>
                    </div>
                </div>
            </div><!-- /.card-system -->
        
            
            <div class="card-system">
                <div class="card-system-inner">
                    <div class="card-system-image" data-background-image="../assets/img/tmp/product-4.jpg">
                        <a href="listing-detail.html"></a>
                    </div><!-- /.card-system-image -->

                    <div class="card-system-content">
                        <h2><a href="listing-detail.html">Coffee &amp; Newspaper</a></h2>
                        <h3>Posted sever hours ago</h3>
                        <a href="#" class="btn btn-primary btn-xs">Edit</a>
                        <a href="#" class="btn btn-secondary btn-xs">Ban</a>
                    </div>
                </div>
            </div><!-- /.card-system -->
        
            
            <div class="card-system">
                <div class="card-system-inner">
                    <div class="card-system-image" data-background-image="../assets/img/tmp/product-5.jpg">
                        <a href="listing-detail.html"></a>
                    </div><!-- /.card-system-image -->

                    <div class="card-system-content">
                        <h2><a href="listing-detail.html">Boat Trip</a></h2>
                        <h3>Posted sever hours ago</h3>
                        <a href="#" class="btn btn-primary btn-xs">Edit</a>
                        <a href="#" class="btn btn-secondary btn-xs">Ban</a>
                    </div>
                </div>
            </div><!-- /.card-system -->
        
    </div><!-- /.cards-system -->
</div>

        </div><!-- /.col-* -->
    </div><!-- /.row -->
</div><!-- /.col-* -->

                                    </div>
                                </div><!-- /.container-fluid -->
                            </div><!-- /.content-admin-main-inner -->
                        </div><!-- /.content-admin-main -->

                        <div class="content-admin-footer">
                            <div class="container-fluid">
                                <div class="content-admin-footer-inner">
                                    &copy; 2015 All rights reserved. Created by <a href="#">Aviators</a>.
                                </div><!-- /.content-admin-footer-inner -->
                            </div><!-- /.container-fluid -->
                        </div><!-- /.content-admin-footer  -->
                    </div><!-- /.content-admin-wrapper -->
                </div><!-- /.content-admin -->
            </div><!-- /.wrapper-admin -->
        </div><!-- /.outer-admin -->
    </div><!-- /.main -->
@include("include/user/bottom")