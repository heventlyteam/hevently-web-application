@extends('include.header')
@section('header_c')
    <div class="main">
        <div class="main-inner">
            <div class="container">
                <div class="content">
                    


                    <div class="row">
    <div class="col-sm-4 col-sm-offset-4" style="background:#fefefe;padding:20px;">
        <div class="page-title">
            <h3 class="text-center">Welcome Back, Login</h3>
        </div><!-- /.page-title -->

        <form method="post" action="">
            @if(isset($data) && count($data) > 0)
                <div class="">
                    {!! $data['reason'] !!}
                </div>
            @endif
            {!! csrf_field() !!}
            <div class="form-group">
                <label for="login-form-email">E-mail</label>
                <input type="email" class="form-control" name="email" id="login-form-email">
            </div><!-- /.form-group -->

            <div class="form-group">
                <label for="login-form-password">Password</label>
                <input type="password" class="form-control" name="password" id="login-form-password">
            </div><!-- /.form-group -->
            {!! csrf_field() !!}
            <div class="checkbox">
                <input type="checkbox" id="remember"><label for="remember">Remember Me</label>
            <a href="pasword/recovery" class="pull-right">Forgot Password? </a>
            </div>
            

            <button type="submit" class="btn btn-primary btn-block text-center">Login</button>
            <hr />
            <p class="text-center">Don't have an Account?</p>
            <div class="text-center"><a href="register.php">Create an Account</a></div>
            <hr>
            <p class="text-center">Re-send activation link?</p>
            <div class="text-center"><a href="account/re-send">Activation code</a></div>
        </form>
    </div><!-- /.col-sm-4 -->
</div><!-- /.row -->

                </div><!-- /.content -->
            </div><!-- /.container -->
        </div><!-- /.main-inner -->
    </div><!-- /.main -->
@endsection