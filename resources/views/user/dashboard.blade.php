<?php
$check = new \App\Http\Controllers\LoginSessionManagement();
$checkUser = $check->validateLoginSession('userinfo');
$checkProvider = $check->validateLoginSession('sp_info');
$sessionInfo = new \Illuminate\Support\Facades\Session();
$userInfo = $sessionInfo::get('sp_info');
$providerInfo = $infoFom['provider']['data'];

?>
@include("include/user/top")
    <?php

    if($sessionInfo::has('status')){
        $status = json_decode($sessionInfo::get('status'), true);
        if($status['success'])
            $st = 'true';
        else
            $st = 'false';
        echo '<script type="text/javascript">
            $(document).ready(function(){
            alert();
                loadViewDP();

                displayError("'.$status['message'].'", '.$st.');
            });
        </script>
    ';
        $sessionInfo::forget('status');
    }
    ?>
    <header class="header header-minimal">
    <div class="header-wrapper">
        <div class="container-fluid">
            <div class="header-inner">
                <div class="header-logo">
                    <a href="">
                        <img src="../assets/img/logo.png" alt="Logo">
                    </a>
                </div><!-- /.header-logo -->

                <div class="header-content">
                    <div class="header-bottom">


<button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target=".header-nav-primary">
    <span class="sr-only">Toggle navigation</span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
</button>


                        <div class="header-nav-user">
    <div class="dropdown">
        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
            <div class="user-image">
                <img src="../assets/img/tmp/agent-2.jpg">
                <div class="notification"></div><!-- /.notification-->
            </div><!-- /.user-image -->
            <span class="header-nav-user-name">{!! $userInfo->username !!}</span> <i class="fa fa-chevron-down"></i>
        </button>

        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
            <li><a href="#user-profile-edit" onclick="load_edit_profile()">Edit Profile</a></li>
            <li><a href="#sp/view/dp" onclick="loadViewDP()">View/Edit Display Pictures</a></li>
            <li><a href="#sp/cngpassword" onclick="loadPasswordCng()">Change Password</a></li>
            <li><a href="provider/logout">Logout</a></li>
        </ul>
    </div><!-- /.dropdown -->
</div><!-- /.header-nav-user -->

                    </div><!-- /.header-bottom -->
                </div><!-- /.header-content -->
            </div><!-- /.header-inner -->
        </div><!-- /.container -->
    </div><!-- /.header-wrapper -->

    <div class="header-statusbar" style="padding:18px;">
        <div class="header-statusbar-inner" style="padding:0px;">
            <div class="header-statusbar-left">
                <h1>Dashboard ({!! $providerInfo['bizname'] !!})</h1>
            </div><!-- /.header-statusbar-left -->

            <div class="header-statusbar-right">
                <div class="hidden-xs visible-lg">
                    Navigation:
                    <ul class="breadcrumb">
                        <li><a href="#">Hevently</a></li>
                        <li><a href="#" onclick="loadDashboard()">Dashboard</a></li>
                    </ul>
                </div>
            </div><!-- /.header-statusbar-right -->
        </div><!-- /.header-statusbar-inner -->
    </div><!-- /.header-statusbar -->
</header><!-- /.header -->




    <div class="main">
        <div class="outer-admin">
            <div class="wrapper-admin">
                @include("include.user.sidebar-primary")
                <div class="content-admin">
                    <div class="content-admin-wrapper">
                        <div class="content-admin-main">
                            <div class="content-admin-main-inner">
                                <div class="container-fluid">
                                    <div class="row main_row2"></div>

                                    <script type="text/javascript">
                                        //Autocomplete variables
                                        var input = document.getElementById('address');
                                        var lon2 = document.getElementById('long');
                                        var lat2 = document.getElementById('lat');
                                        var searchform = document.getElementById('form1');
                                        var place;
                                        var autocomplete = new google.maps.places.Autocomplete(input);

                                        //Google Map variables
                                        var map;
                                        var marker;

                                        //Add listener to detect autocomplete selection
                                        google.maps.event.addListener(autocomplete, 'place_changed', function () {
                                            place = autocomplete.getPlace();
                                            console.log(place.geometry.location.lat());
                                            lat2.value = place.geometry.location.lat();
                                            lon2.value = place.geometry.location.lng();
                                        });
                                        //Add listener to search
                                        searchform.addEventListener("submit", function() {
                                            var newlatlong = new google.maps.LatLng(place.geometry.location.lat(),place.geometry.location.lng());
                                            map.setCenter(newlatlong);
                                            marker.setPosition(newlatlong);
                                            map.setZoom(12);

                                        });

                                        //Reset the inpout box on click
                                        input.addEventListener('click', function(){
                                            input.value = "";

                                        });



                                        function initialize() {
                                            var myLatlng = new google.maps.LatLng(51.517503,-0.133896);
                                            var mapOptions = {
                                                zoom: 1,
                                                center: myLatlng
                                            }
                                            map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

                                            marker = new google.maps.Marker({
                                                position: myLatlng,
                                                map: map,
                                                title: 'Main map'
                                            });
                                        }

                                        google.maps.event.addDomListener(window, 'load', initialize);

                                    </script>
                                    <div class="row main_row">
                                        <div class="col-sm-12">
    <!-- The Account info -->
    <div class="container2">
        <div class="col-lg-12 self-dist" style="background-color: #FFFFFF; border: thin">
           <fieldset>
               <legend><h2>My Account Details</h2></legend>
               </fieldset>
               <div class="row">
                   <div class="col-sm-12 col-lg-3 account_det">
                       <div class="panel panel-default">
                           <div class="panel-heading">
                               Total Credit
                           </div>
                           <div class="panel-body">
                               ${!! $providerInfo['credit']  !!}
                           </div>
                       </div>
                   </div>
                   <div class="col-sm-12 col-lg-3 account_det">
                       <div class="panel panel-default">
                           <div class="panel-heading">
                               Likes
                           </div>
                           <div class="panel-body">
                               {!! count($providerInfo['love']) !!}
                           </div>
                       </div>
                   </div>
                   <div class="col-sm-12 col-lg-3 account_det">

                       <div class="panel panel-default">
                           <div class="panel-heading">
                               Service
                           </div>
                           <div class="panel-body">
                               {!! $providerInfo['category'] !!}
                           </div>
                       </div>
                   </div>
                   <div class="col-sm-12 col-lg-3 account_det">

                       <div class="panel panel-default">
                           <div class="panel-heading">
                               Verification
                           </div>
                           <div class="panel-body">
                               <div class="label label-{!! $providerInfo['verified'] == 1 ? 'success':'danger' !!} button">{!! $providerInfo['verified'] == 1? 'Verified': 'Unverified'!!}</div>
                           </div>
                       </div>
                   </div>
                </div>
               <div class="row">
                   <div class="col-sm-12 col-lg-3 account_det">
                       <div class="panel panel-default">
                           <div class="panel-heading">
                               Email
                           </div>
                           <div class="panel-body">
                               {!! $providerInfo['email'] !!}
                           </div>
                       </div>
                   </div>
                   <div class="col-sm-12 col-lg-3 account_det">
                       <div class="panel panel-default">
                           <div class="panel-heading">
                               Profile Link
                           </div>
                           <div class="panel-body">
                               provider/{!! $providerInfo['link'] !!}
                           </div>
                       </div>
                   </div>
                   <div class="col-sm-12 col-lg-3 account_det">

                       <div class="panel panel-default">
                           <div class="panel-heading">
                               COntect Number
                           </div>
                           <div class="panel-body">
                               {!! $providerInfo['phone'] !!}
                           </div>
                       </div>
                   </div>
                   <div class="col-sm-12 col-lg-3 account_det">

                       <div class="panel panel-default">
                           <div class="panel-heading">
                               Total Offers / <div class="label label-success">new</div>
                           </div>
                           <div class="panel-body">
                              Unavailable/<div class="label label-success button"></div>
                           </div>
                       </div>
                   </div>
                </div>
         </div>

     </div>
     <!-- account info ends-->
    <div class="row">
        <div class="col-sm-12 col-lg-12">
            <div class="row">
                <div class="col-sm-3">
                    <div class="statusbox caution-height">
                        <h2>Profile</h2>
                        <div class="statusbox-content">
                            <strong>25,000 views</strong>
                            <span>Updated 27/04/2015</span>
                        </div><!-- /.statusbox-content -->

                        <div class="statusbox-actions">
                            <a href="#"  data-toggle="tooltip" title="Edit your profile and attract visitors!"><i class="fa fa-eye"></i></a>
                            <script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});
</script>
                        </div><!-- /.statusbox-actions -->

                    </div><!-- /.statusbox -->
                </div>

                <div class="col-sm-3">
                    <div class="statusbox">
                        <h2>Hevently Subscription</h2>
                        <div class="statusbox-content">
                            <strong>13,200 +<code>1</code> points</strong>
                            <span>Subscribed on: 27/04/2015</span>
                            <span>Subscription valid till: 27/04/2015</span>
                        </div><!-- /.statusbox-content -->

                        <div class="statusbox-actions">
                            <a href="#" data-toggle="tooltip" title="Subcribe to Hevently"><i class="fa fa-eye"></i></a>
                            <a href="#"><i class="fa fa-bar-chart"></i></a>
                            <a href="#"><i class="fa fa-share-alt"></i></a>

                        </div><!-- /.statusbox-actions -->
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="statusbox">
                        <h2>Progress</h2>
                        <div class="statusbox-content">
                            <strong>$13,200</strong>
                            <span>Updated 27/04/2015</span>
                        </div><!-- /.statusbox-content -->

                        <div class="statusbox-actions">
                            <a href="#"><i class="fa fa-eye"></i></a>
                            <a href="#"><i class="fa fa-bar-chart"></i></a>
                            <a href="#"><i class="fa fa-share-alt"></i></a>
                        </div><!-- /.statusbox-actions -->
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="statusbox">
                        <h2>Progress</h2>
                        <div class="statusbox-content">
                            <strong>$13,200</strong>
                            <span>Updated 27/04/2015</span>
                        </div><!-- /.statusbox-content -->

                        <div class="statusbox-actions">
                            <a href="#"><i class="fa fa-eye"></i></a>
                            <a href="#"><i class="fa fa-bar-chart"></i></a>
                            <a href="#"><i class="fa fa-share-alt"></i></a>
                        </div><!-- /.statusbox-actions -->
                    </div>
                </div>
            </div><!-- /.row -->
        </div><!-- /.col-* -->

       </div><!-- /.row -->


        </div><!-- /.col-* -->

                                    </div>
                                </div><!-- /.container-fluid -->
                            </div><!-- /.content-admin-main-inner -->
                        </div><!-- /.content-admin-main -->

                        <div class="content-admin-footer">
                            <div class="container-fluid">
                                <div class="alert alert-danger">
                                </div>
                                <div class="content-admin-footer-inner">
                                    &copy; 2015 All rights reserved. Created by <a href="#">Aviators</a>.
                                </div><!-- /.content-admin-footer-inner -->
                            </div><!-- /.container-fluid -->
                        </div><!-- /.content-admin-footer  -->
                    </div><!-- /.content-admin-wrapper -->
                </div><!-- /.content-admin -->
            </div><!-- /.wrapper-admin -->
        </div><!-- /.outer-admin -->
    </div><!-- /.main -->
<div class="outer-loader">
    <div class="inner-loader">
        <div class="loader">
            <img src="assets/img/ajax_loaders/ajax-loader (1).gif" alt="loading..." /> loading
        </div>
    </div>
</div>

<div class="outer-loader" style="z-index: 1000;">
    <div class="inner-loader" style="padding-bottom: 13px;">
        <div class="loader text-center">
            <img src="assets/img/ajax_loaders/ajax-loader (1).gif" alt="loading..." />
            <span class="text-info" style="color:white;">Loading...</span>
        </div>
    </div>
</div>
@include("include/user/bottom")