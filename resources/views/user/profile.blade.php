@include("include/user/top")
    <header class="header header-minimal">
    <div class="header-wrapper">
        <div class="container-fluid">
            <div class="header-inner">
                <div class="header-logo">
                    <a href="index-2.html">
                        <img src="../assets/img/logo.png" alt="Logo">
                    </a>
                </div><!-- /.header-logo -->

                <div class="header-content">
                    <div class="header-bottom">
       

<button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target=".header-nav-primary">
    <span class="sr-only">Toggle navigation</span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
</button>


                        <div class="header-nav-user">
    <div class="dropdown">
        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
            <div class="user-image">
                <img src="../assets/img/tmp/agent-2.jpg">
                <div class="notification"></div><!-- /.notification-->
            </div><!-- /.user-image -->

            <span class="header-nav-user-name">Natasha Samson</span> <i class="fa fa-chevron-down"></i>
        </button>

        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
            <li><a href="user-profile-edit.html">Edit Profile</a></li>
            <li><a href="listing-submit.html">Submit Listing</a></li>
            <li><a href="change-password.html">Change Password</a></li>
        </ul>
    </div><!-- /.dropdown -->
</div><!-- /.header-nav-user -->

                    </div><!-- /.header-bottom -->
                </div><!-- /.header-content -->
            </div><!-- /.header-inner -->
        </div><!-- /.container -->
    </div><!-- /.header-wrapper -->

    <div class="header-statusbar" style="padding:18px;">
        <div class="header-statusbar-inner" style="padding:0px;">
            <div class="header-statusbar-left">
                <h1>Profile</h1>                
            </div><!-- /.header-statusbar-left -->

            <div class="header-statusbar-right">
                <div class="hidden-xs visible-lg">
                    Navigation:
                    <ul class="breadcrumb">
                        <li><a href="#">Hevently</a></li>
                        <li><a href="#">Dashboard</a></li>
                    </ul>
                </div>
            </div><!-- /.header-statusbar-right -->
        </div><!-- /.header-statusbar-inner -->
    </div><!-- /.header-statusbar -->
</header><!-- /.header -->




    <div class="main">
        <div class="outer-admin">
            <div class="wrapper-admin">
                @include("include/user/sidebar-primary")
                @include("include/user/sidebar-secondary")
                <div class="content-admin">
                    <div class="content-admin-wrapper">
                        <div class="content-admin-main">
                            <div class="content-admin-main-inner">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="background-white p20 mb50">
                                                        <h2 class="page-title">About Your Company</h2>

                                                        <form>
                                                            <div class="form-group">
                                                                <label for="exampleInputText1">Company Name</label>
                                                                <input type="text" class="form-control" id="exampleInputText1" placeholder="Your Name">
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="form-group">
                                                                    <label for="login-form-email">Category</label>
                                                                    <select title="Category">
                                                                        <option>Caterers</option>
                                                                        <option>Photographers</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="exampleInputEmail1">Phone Number</label>
                                                                <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Your Email">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="exampleInputEmail1">Address</label>
                                                                <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Your Email">
                                                            </div>
                                                             

                                                            <div class="form-group">
                                                                <label for="exampleInputEmail1">Email</label>
                                                                <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Your Email">
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="exampleInputTextarea">About Your Company ( 150 character maximum)</label>
                                                                <textarea class="form-control" rows="5" id="exampleInputTextarea" placeholder="Some Sweet Words"></textarea>
                                                            </div>

                                                            <button type="submit" class="btn btn-primary"><i class="fa fa-comment"></i> Submit</button>
                                                        </form>
                                                    </div>
                                                </div>

                                            </div><!-- /.row -->
                                        </div><!-- /.col-* -->
                                    </div>
                                </div><!-- /.container-fluid -->
                            </div><!-- /.content-admin-main-inner -->
                        </div><!-- /.content-admin-main -->

                        <div class="content-admin-footer">
                            <div class="container-fluid">
                                <div class="content-admin-footer-inner">
                                    &copy; 2015 All rights reserved. <a href="#">Hevently.com</a>.
                                </div><!-- /.content-admin-footer-inner -->
                            </div><!-- /.container-fluid -->
                        </div><!-- /.content-admin-footer  -->
                    </div><!-- /.content-admin-wrapper -->
                </div><!-- /.content-admin -->
            </div><!-- /.wrapper-admin -->
        </div><!-- /.outer-admin -->
    </div><!-- /.main -->
@include("include/user/bottom")